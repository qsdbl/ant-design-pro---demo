# 简介

基于 Ant Design Pro 平台而开发的demo，一个前后端分离项目。

测试账号：

- 用户名：admin
- 密码：ant.design



## 前端项目技术栈

- Node
- Npm
- React
- Ant Design
- Ant Design Pro



## 后端项目技术栈

- SpringBoot 2.4.5
- MyBatis 2.1.4
- Druid 1.1.21
- hutool 5.7.16
- JDK 1.8



# 项目预览

![登录页面](https://gitee.com/qsdbl/markdown_img/raw/master/2021/202204121135990.jpg)

![树形数据](https://gitee.com/qsdbl/markdown_img/raw/master/2021/202204121136064.jpg)

![数据选择](https://gitee.com/qsdbl/markdown_img/raw/master/2021/202204121136917.jpg)

![编辑](https://gitee.com/qsdbl/markdown_img/raw/master/2021/202204121142157.jpg)









