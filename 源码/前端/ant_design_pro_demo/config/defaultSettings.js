const Settings =
  // {
  //   menu: {
  //     locale: false,
  //   },
  //   navTheme: 'light',
  //   primaryColor: '#1890ff',
  //   layout: 'top',
  //   contentWidth: 'Fluid',
  //   fixedHeader: false,
  //   fixSiderbar: true,
  //   pwa: false,
  //   title: '东宏智控',
  //   logo: 'https://zhengxin-pub.cdn.bcebos.com/logopic/684e2149e4740fc56f5247c261c6144b_fullsize.jpg?x-bce-process=image/resize,m_lfit,w_200',
  //   headerHeight: 48,
  //   splitMenus: false,
  // };

  {
    navTheme: 'light',
    // 拂晓蓝
    menu: {
      locale: false,
    },
    primaryColor: '#1890ff',
    layout: 'side',
    contentWidth: 'Fluid',
    fixedHeader: false,
    fixSiderbar: true,
    colorWeak: false,
    title: '东宏智控',
    pwa: false,
    logo: 'https://zhengxin-pub.cdn.bcebos.com/logopic/684e2149e4740fc56f5247c261c6144b_fullsize.jpg?x-bce-process=image/resize,m_lfit,w_200',
    iconfontUrl: '',
  };

export default Settings;
