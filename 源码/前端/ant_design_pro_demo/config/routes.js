export default [
  {
    path: '/user',
    layout: false,
    routes: [
      {
        path: '/user',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './user/Login',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/',
    redirect: '/store/rec',
  },

  {
    name: '仓库管理',
    icon: 'table',
    routes: [
      {
        name: '入库管理',
        icon: 'smile',
        path: '/store/store_in',
        component: './Store/store_in',
      },
      {
        name: '新增入库单',
        icon: 'smile',
        path: '/store/store_in/add',
        hideInMenu: true,
        component: './Store/store_in/add',
      },
      {
        name: '入库单明细',
        icon: 'smile',
        path: '/store/store_in/det',
        hideInMenu: true,
        component: './Store/store_in/det',
      },
      {
        name: '出库管理',
        icon: 'smile',
        path: '/store/store_out',
        component: './Store/store_out',
      },
      {
        name: '新增出库单',
        icon: 'smile',
        path: '/store/store_out/add',
        hideInMenu: true,
        component: './Store/store_out/add',
      },
      {
        name: '库存查询',
        icon: 'smile',
        path: '/store/rec',
        component: './Store/store_rec',
      },
      {
        name: '物资目录',
        icon: 'smile',
        path: '/store/mat_list',
        component: './Store/mat_list',
      },
      {
        name: '仓库目录',
        icon: 'smile',
        path: '/store/house_list',
        component: './Store/house_list',
      },
      {
        name: '物资分类',
        icon: 'smile',
        path: '/store/mat_sort',
        component: './Store/mat_sort',
      },
    ],
  },
  // {
  //   name: '库存管理',
  //   icon: 'table',
  //   routes: [
  //     {
  //       name: '仓库业务',
  //       icon: 'smile',
  //       routes: [
  //         {
  //           name: '入库管理',
  //           icon: 'smile',
  //           path: '/store/store_in',
  //           component: './Store/store_in',
  //         },
  //         {
  //           name: '新增入库单',
  //           icon: 'smile',
  //           path: '/store/store_in/add',
  //           hideInMenu: true,
  //           component: './Store/store_in/add',
  //         },
  //         {
  //           name: '出库管理',
  //           icon: 'smile',
  //           path: '/store/store_out',
  //           component: './Store/store_out',
  //         },
  //         {
  //           name: '新增出库单',
  //           icon: 'smile',
  //           path: '/store/store_out/add',
  //           hideInMenu: true,
  //           component: './Store/store_out/add',
  //         },
  //         {
  //           name: '库存查询',
  //           icon: 'smile',
  //           path: '/store/rec',
  //           component: './Store/store_rec',
  //         },
  //       ],
  //     },
  //     {
  //       name: '仓库基础',
  //       icon: 'smile',
  //       routes: [
  //         {
  //           name: '物资目录',
  //           icon: 'smile',
  //           path: '/store/mat_list',
  //           component: './Store/mat_list',
  //         },
  //         {
  //           name: '仓库目录',
  //           icon: 'smile',
  //           path: '/store/house_list',
  //           component: './Store/house_list',
  //         },
  //         {
  //           name: '物资分类',
  //           icon: 'smile',
  //           path: '/store/mat_sort',
  //           component: './Store/mat_sort',
  //         },
  //         {
  //           name: '仓库',
  //           icon: 'smile',
  //           path: '/store/base/repo',
  //           component: './Store/base/repo_list',
  //         },
  //       ],
  //     },
  //   ],
  // },
  {
    hideInMenu: true, //隐藏
    path: '/welcome',
    name: 'welcome',
    icon: 'smile',
    component: './Welcome',
  },
  {
    path: '/admin',
    name: 'admin',
    icon: 'crown',
    access: 'canAdmin',
    component: './Admin',

    routes: [
      {
        path: '/admin/sub-page',
        name: 'sub-page',
        icon: 'smile',
        component: './Welcome',
      },
      {
        component: './404',
      },
    ],
  },
  {
    component: './404',
  },
];
