import { PageContainer } from '@ant-design/pro-layout';
import {
  Table,
  Button,
  Input,
  Space,
  Card,
  Divider,
  Row,
  Col,
  Modal,
  Form,
  DatePicker,
  AutoComplete,
  Popconfirm,
  message,
} from 'antd';
import './index.less';
const { Search } = Input;
import { getHouses, addHouse, updateHouse, delHouse } from '@/services/store';
import React, { useState, useEffect } from 'react';
import moment from 'moment';

export default (props) => {
  //form表单实例
  const [myform] = Form.useForm();
  //控制 弹窗
  const [isModalVisible, setIsModalVisible] = useState(false);
  //弹窗数据
  const [houseInfo, setHouseInfo] = useState({});

  //库存数据
  const [storeRecOld, setStoreRecOld] = useState([]);
  const [storeRec, setStoreRec] = useState([]);
  //数据加载状态
  const [loading, setLoading] = useState(false);
  //分页参数
  const [pagination, setPagination] = useState({
    currentPage: 1,
    pageSize: 10,
    total: 0,
  });

  //加载数据（只加载一次）
  useEffect(() => {
    fetchData();
  }, []);

  //初次加载数据（查询100条）
  const fetchData = async () => {
    const res = await getHouses({
      pagination: {
        currentPage: 1,
        pageSize: 100, //查询100条
        total: 0,
      },
    });
    setStoreRec(() => res.data);
    setStoreRecOld(() => res.data);
    setPagination(() => {
      return {
        // pageSize: res.data.pageSize,
        pageSize: 10, //一页显示10条
        currentPage: res.data.currentPage,
        total: res.data.total,
      };
    });
  };
  //重置按钮
  const resetBtn = () => {
    myform.resetFields();
    setStoreRec(() => storeRecOld);
  };

  //新增按钮
  const add = () => {
    // console.log('新增');
    setHouseInfo(() => {
      return {
        module_type: 'add', //弹窗类型 新增
        house_name: '',
        house_code: '',
        house_desc: '',
        add_date: moment(new Date(), 'YYYY/MM/DD HH:mm:ss'),
      };
    });
    setIsModalVisible(true);
  };

  //编辑 按钮
  const handleEdit = (record) => {
    // console.log('编辑 按钮');
    // console.log(record);
    record.module_type = 'edit'; //弹窗类型 编辑
    setHouseInfo(() => record);
    setIsModalVisible(true);
  };

  //分页 数据获取
  const handleTableChange = (mypagination) => {
    let pg = {
      pagination: {
        pageSize: mypagination.pageSize,
        currentPage: mypagination.current,
      },
    };
    getHouses(pg).then((res) => {
      setStoreRec(res.data);
      setStoreRecOld(res.data);
      setPagination({
        pageSize: res.data.pageSize,
        currentPage: res.data.currentPage,
        total: res.data.total,
      });
    });
  };
  //表格 列名
  const columns = [
    {
      title: '仓库名称',
      dataIndex: 'house_name',
    },
    {
      title: '仓库编码',
      dataIndex: 'house_code',
    },
    {
      title: '创建时间',
      dataIndex: 'add_date',
    },
    {
      title: '描述',
      dataIndex: 'house_desc',
    },
    {
      title: '操作',
      dataIndex: '操作',
      render: (text, record) => (
        <Space size="middle">
          <a onClick={handleEdit.bind(this, record)}>编辑</a>
          <Popconfirm
            title="确认要删除该仓库？"
            onConfirm={confirm.bind(this, record)}
            onCancel={cancel}
            okText="确认"
            cancelText="取消"
          >
            <a style={{ color: 'red' }}>删除</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];
  //确认删除
  const confirm = (record) => {
    delHouse(record.house_id)
      .then((res) => {
        //重新获取数据
        fetchData();
        if (res.success != 'true') {
          //删除失败
          message.error('删除失败！' + res.message);
        } else {
          //删除成功
          message.success('删除成功！');
        }
      })
      .catch(() => {
        message.error('网络异常！');
      });
  };
  //取消删除
  const cancel = (record) => {
    message.error('已取消！');
  };

  //提交
  const onFinish = (values) => {
    values.add_date = moment(values.add_date).format('YYYY/MM/DD HH:mm:ss');
    if (houseInfo.module_type == 'add') {
      //新增
      addHouse(values)
        .then((res) => {
          //重新获取数据
          fetchData();
          if (res.success != 'true') {
            //添加失败
            message.error('添加失败！' + res.message);
          } else {
            //添加成功
            message.success('添加成功！');
          }
        })
        .catch(() => {
          message.error('网络异常！');
        });
    } else {
      //编辑
      values.house_id = houseInfo.house_id;
      updateHouse(values)
        .then((res) => {
          //重新获取数据
          fetchData();
          if (res.success != 'true') {
            //修改失败
            message.error('修改失败！' + res.message);
          } else {
            //修改成功
            message.success('修改成功！');
          }
        })
        .catch(() => {
          message.error('网络异常！');
        });
    }
    setIsModalVisible(false);
  };
  //取消
  const myCancel = () => {
    setIsModalVisible(false);
  };

  //监听 所有输入框（搜索框 - 前端分页）
  const onChange = (allFields) => {
    let fieldsValue = myform.getFieldsValue(true); //各搜索框的 输入值
    let mydataList = []; //存放搜索结果
    let dataList_now = JSON.parse(JSON.stringify(storeRecOld)); //表格全部数据
    let fields_search = ['house_desc', 'add_date', 'house_code', 'house_name']; //要搜索的字段
    let isNotNull = false; //输入框是否为空
    for (let key in fieldsValue) {
      let search = fieldsValue[key]; //搜索框中输入的数据
      if (fields_search.indexOf(key) != -1 && search != '') {
        mydataList = [];
        isNotNull = true;
        mydataList.push(
          ...dataList_now.filter((data) =>
            (data[key] + '').toLowerCase().includes(search.toLowerCase()),
          ),
        );
        dataList_now = JSON.parse(JSON.stringify(mydataList)); //更新dataList_now
      }
    }
    if (mydataList.length == 0 && !isNotNull) {
      //输入框为空
      fetchData(); //重新加载数据
    } else {
      setStoreRec(() => mydataList);
      setPagination(() => {
        //设置分页
        return {
          currentPage: 1,
          pageSize: 10,
          total: 0,
        };
      });
    }
  };

  return (
    <PageContainer>
      <Space direction="vertical" style={{ width: '100%' }} size="large">
        <Card bordered={false}>
          <Divider orientation="left" style={{ color: '#1890ff' }}>
            搜索
          </Divider>
          <Form
            form={myform}
            onFieldsChange={(_, allFields) => {
              onChange(allFields);
            }}
          >
            <Row gutter={16}>
              <Col className="gutter-row" span={5}>
                <Form.Item name="house_name">
                  <Search placeholder="仓库名称" allowClear />
                </Form.Item>
              </Col>
              <Col className="gutter-row" span={5}>
                <Form.Item name="house_code">
                  <Search placeholder="仓库编码" allowClear />
                </Form.Item>
              </Col>
              <Col className="gutter-row" span={5}>
                <Form.Item name="add_date">
                  <Search placeholder="创建时间" allowClear />
                </Form.Item>
              </Col>
              <Col className="gutter-row" span={5}>
                <Form.Item name="house_desc">
                  <Search placeholder="描述" allowClear />
                </Form.Item>
              </Col>
              <Col className="gutter-row" span={4}>
                <Space size="large">
                  <Button type="primary" danger onClick={resetBtn}>
                    重置
                  </Button>
                  <Button type="primary" onClick={add}>
                    新增
                  </Button>
                </Space>
              </Col>
            </Row>
          </Form>
        </Card>
        <Card bordered={false}>
          <Divider orientation="left" style={{ color: '#1890ff' }}>
            数据
          </Divider>
          <Table
            columns={columns}
            rowKey={(record) => record.house_code}
            dataSource={storeRec}
            pagination={pagination}
            loading={loading}
            onChange={handleTableChange}
          />
        </Card>
        <Modal
          title={houseInfo.module_type == 'add' ? '新增' : '编辑'}
          centered
          visible={isModalVisible}
          footer={null}
          width={700}
          destroyOnClose
          onCancel={myCancel}
        >
          <Form
            preserve={false}
            name="basic"
            initialValues={{
              house_name: houseInfo.house_name,
              house_code: houseInfo.house_code,
              house_desc: houseInfo.house_desc,
              add_date: moment(houseInfo.add_date, 'YYYY/MM/DD HH:mm:ss'),
            }}
            //提交表单且数据验证成功后
            onFinish={onFinish}
            autoComplete="off"
          >
            <Form.Item
              label="仓库名称"
              name="house_name"
              rules={[
                {
                  required: true,
                  message: '仓库名称必填！',
                },
              ]}
            >
              <AutoComplete
                allowClear
                placeholder={houseInfo.house_name}
                options={[
                  {
                    label: houseInfo.house_name,
                    value: houseInfo.house_name,
                  },
                ]}
              />
            </Form.Item>
            <Form.Item
              label="仓库编码"
              name="house_code"
              rules={[
                {
                  required: true,
                  message: '仓库编码必填！',
                },
              ]}
            >
              <AutoComplete
                allowClear
                placeholder={houseInfo.house_code}
                options={[
                  {
                    label: houseInfo.house_code,
                    value: houseInfo.house_code,
                  },
                ]}
              />
            </Form.Item>
            <Form.Item
              label="添加时间"
              name="add_date"
              rules={[
                {
                  required: true,
                  message: '添加时间必填！',
                },
              ]}
            >
              <DatePicker disabled showTime format="YYYY/MM/DD HH:mm:ss" />
            </Form.Item>
            <Form.Item
              label="仓库描述"
              name="house_desc"
              rules={[
                {
                  required: true,
                  message: '仓库描述必填！',
                },
              ]}
            >
              <AutoComplete
                allowClear
                placeholder={houseInfo.house_desc}
                options={[
                  {
                    label: houseInfo.house_desc,
                    value: houseInfo.house_desc,
                  },
                ]}
              />
            </Form.Item>
            <Form.Item>
              <Space size="large">
                <Button type="danger" htmlType="submit" style={{ width: '10vw' }}>
                  确认
                </Button>
                <Button
                  type="primary"
                  htmlType="reset"
                  style={{ width: '10vw' }}
                  onClick={myCancel}
                >
                  取消
                </Button>
              </Space>
            </Form.Item>
          </Form>
        </Modal>
      </Space>
    </PageContainer>
  );
};
