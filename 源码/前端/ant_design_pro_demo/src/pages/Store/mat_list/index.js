import { PageContainer } from '@ant-design/pro-layout';
import {
  Table,
  Button,
  Input,
  Space,
  Card,
  Row,
  Col,
  Modal,
  Form,
  Popconfirm,
  DatePicker,
  message,
  Tree,
} from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import './index.less';
import React, { useState, useEffect } from 'react';
import { getMatTree, updateMat, addMat, deleteMat, getMats } from '@/services/store';
const { Search, TextArea } = Input;
import moment from 'moment';

export default (props) => {
  //物资目录（树形）
  const [matTree, setMatTree] = useState([]);
  const [matTreeOld, setMatTreeOld] = useState([]);
  const [matList, setMatList] = useState([]);
  //分页参数
  const [pagination, setPagination] = useState({
    currentPage: 1,
    pageSize: 8,
  });
  //控制 弹窗
  const [isModalVisible, setIsModalVisible] = useState(false);
  //弹窗数据
  const [matInfo, setMatInfo] = useState({});

  //初次获取数据
  useEffect(() => {
    loadData();
  }, []);

  //加载页面数据
  const loadData = () => {
    //重新加载数据
    getMatTree().then((res) => {
      setMatTree(res.data);
      setMatTreeOld(res.data);
      onSelect('', {
        node: res.data[0],
      });
    });
  };
  //加载 物资数据
  const loadData_mats = (data) => {
    // data-节点信息，需包含type_name
    getMats(data.type_name).then((resMat) => {
      setMatList((data) => {
        return [...data, ...resMat.data];
      });
    });
    if (data.children && data.children.length > 0) {
      //若存在 子节点，一并查询
      data.children.forEach((item) => {
        loadData_mats(item); //递归
      });
    }
  };
  //获取随机数。参数n为随机数的长度。
  const getRandomNumber = (n) => {
    var arr = new Array(n); //用于存放随机数
    var randomNumber = ''; //存放随机数
    for (var i = 0; i < arr.length; i++) arr[i] = parseInt(Math.random() * 10);
    var flag = 0;
    for (var i = 0; i < arr.length - 1; i++) {
      for (var j = i + 1; j < arr.length; j++) {
        if (arr[i] == arr[j]) {
          flag = 1;
          break;
        }
      }
      if (flag) break;
    }
    for (var i = 0; i < arr.length; i++) {
      randomNumber += arr[i];
    }
    return randomNumber;
  };

  //过滤 树
  const searchInput = (search) => {
    if (search.target) {
      // eslint-disable-next-line no-param-reassign
      search = search.target.value;
    }
    //搜索框
    if (search == '') {
      //变量search绑定输入框中输入的数据
      setMatTree(matTreeOld);
    }
    setMatTree(matTreeOld.filter((mtree) => filterNode(search, mtree)));
  };
  const filterNode = (value, data) => {
    //tree过滤（搜索输入框）
    //value - filterText变量的值，data - 节点数据（所有节点都会被过滤一遍）
    if (!value) return true; //搜索输入框为空时（没有输入数据的情况），返回全部的数据
    return data.type_name.indexOf(value) !== -1; //过滤 物资名称，返回物资名称中包含value值的 节点
  };

  //表格 列名
  const columns = [
    {
      title: '分类名称',
      dataIndex: 'typeName',
      render: (text, record) => <span>{text}</span>,
    },
    {
      title: '分类编码',
      dataIndex: 'typeCode',
      render: (text, record) => <span style={{ color: '#1890ff' }}>{text}</span>,
    },
    {
      title: '物资名称',
      dataIndex: 'mat_name',
      render: (text, record) => <span>{text}</span>,
    },
    {
      title: '物资编码',
      dataIndex: 'mat_code',
      render: (text, record) => <span style={{ color: '#1890ff' }}>{text}</span>,
    },
    {
      title: '物资型号',
      dataIndex: 'mat_size',
      render: (text, record) => <span>{text}</span>,
    },
    {
      title: '单位',
      dataIndex: 'mat_unit',
      render: (text, record) => <span>{text}</span>,
    },
    {
      title: '单价（元）',
      dataIndex: 'mat_price',
      render: (text, record) => <span>{parseFloat(text)}</span>,
    },
    {
      title: '创建时间',
      dataIndex: 'add_date',
      render: (text, record) => <span>{text ? (text + '').substring(0, 10) : ''}</span>,
    },
    {
      title: '备注',
      dataIndex: 'mat_desc',
      render: (text, record) => <span>{text}</span>,
    },
    {
      title: '操作',
      dataIndex: '操作',
      render: (text, record) => (
        <Space size="middle">
          <a onClick={handleEdit.bind(this, record)}>编辑</a>
          <Popconfirm
            title="确认要删除该分类？"
            onConfirm={handleDelete.bind(this, record)}
            onCancel={cancel}
            okText="确认"
            cancelText="取消"
          >
            <a style={{ color: 'red' }}>删除</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];
  //新增 按钮
  const add = () => {
    matInfo.module_type = 'add'; //弹窗类型 新增
    // console.log(matInfo);
    setMatInfo(() => {
      return {
        typeName: matInfo.node.type_name,
        typeCode: matInfo.node.type_code,
        type_id: matInfo.node.type_id,
        type_level: matInfo.node.type_level,
        type_desc: matInfo.node.type_desc,
        mat_price: 0,
        add_date: moment(new Date(), 'YYYY/MM/DD HH:mm:ss'),
        node: matInfo.node,
        module_type: (matInfo.module_type = 'add'), //弹窗类型 新增
      };
    });
    setIsModalVisible(true);
  };
  //编辑 按钮
  const handleEdit = (record) => {
    record.module_type = 'edit'; //弹窗类型 编辑
    record.add_date = moment(new Date(record.add_date), 'YYYY/MM/DD HH:mm:ss');
    record.node = matInfo.node;
    setIsModalVisible(true);
    setMatInfo(() => record);
  };

  //删除 按钮
  const handleDelete = (record) => {
    deleteMat(record.mat_id)
      .then((res) => {
        //重新加载数据
        onSelect('', {
          node: matInfo.node,
        });
        if (res.success != 'true') {
          //添加失败
          message.error('删除失败！' + res.message);
        } else {
          //添加成功
          message.success('删除成功！');
        }
      })
      .catch(() => {
        message.error('网络异常！');
      });
  };
  //取消删除
  const cancel = (record) => {
    message.error('已取消！');
  };

  //弹窗：
  //提交
  const onFinish = (values) => {
    values.add_date = moment(values.add_date).format('YYYY/MM/DD HH:mm:ss');
    values.type_id = matInfo.type_id;
    values.mat_id = matInfo.mat_id;
    values.mat_code = matInfo.typeCode + values.mat_code;
    if (matInfo.module_type == 'add') {
      //新增
      addMat(values)
        .then((res) => {
          //重新获取数据
          onSelect('', {
            node: matInfo.node,
          });
          if (res.success != 'true') {
            //添加失败
            message.error('添加失败！' + res.message);
          } else {
            //添加成功
            message.success('添加成功！');
          }
        })
        .catch(() => {
          message.error('网络异常！');
        });
    } else {
      //编辑
      updateMat(values)
        .then((res) => {
          //重新获取数据
          onSelect('', {
            node: matInfo.node,
          });
          if (res.success != 'true') {
            //修改失败
            message.error('修改失败！' + res.message);
          } else {
            //修改成功
            message.success('修改成功！');
          }
        })
        .catch(() => {
          message.error('网络异常！');
        });
    }
    setIsModalVisible(false);
  };

  //取消
  const myCancel = () => {
    //隐藏弹窗
    setIsModalVisible(false);
  };
  //控件被选中回调
  const onSelect = (selectedKeys, info) => {
    setMatList(() => []);
    setMatInfo(() => {
      return {
        node: info.node,
      };
    });
    loadData_mats(info.node);
  };

  return (
    <PageContainer>
      <Space direction="vertical" style={{ width: '100%' }} size="large">
        <Card bordered={false}>
          <Row gutter={16}>
            <Col span={4}>
              <Search
                style={{ marginBottom: 8 }}
                placeholder="输入关键字搜索！"
                onSearch={searchInput}
                onInput={searchInput}
                allowClear
              />
              <Tree
                showLine={{ showLeafIcon: false }}
                blockNode
                treeData={matTree}
                fieldNames={{ title: 'type_name', key: 'type_code', children: 'children' }}
                onSelect={onSelect}
              />
            </Col>
            <Col span={20}>
              <Table
                columns={columns}
                rowKey={(record) => record.typeCode + getRandomNumber(7)}
                dataSource={matList}
                pagination={pagination}
              />
              <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                新增物资
              </Button>
            </Col>
          </Row>
        </Card>
        <Modal
          title={matInfo.module_type == 'add' ? '新增' : '编辑'}
          centered
          visible={isModalVisible}
          width={900}
          footer={null}
          destroyOnClose
          onCancel={myCancel}
        >
          <Form
            preserve={false}
            name="basic"
            initialValues={{
              typeName: matInfo.typeName,
              typeCode: matInfo.typeCode,
              mat_name: matInfo.mat_name,
              mat_code:
                matInfo.module_type == 'add'
                  ? ''
                  : (matInfo.mat_code + '').substring((matInfo.mat_code + '').length - 2),
              mat_size: matInfo.mat_size,
              mat_unit: matInfo.mat_unit,
              mat_price: parseFloat(matInfo.mat_price),
              add_date: matInfo.add_date,
              mat_desc: matInfo.mat_desc,
            }}
            //提交表单且数据验证成功后
            onFinish={onFinish}
            autoComplete="off"
          >
            <Row gutter={24}>
              <Col span={12}>
                <Form.Item label="分类名称" name="typeName">
                  <Input disabled />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="分类编码" name="typeCode">
                  <Input disabled placeholder={matInfo.typeCode} />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col span={12}>
                <Form.Item
                  rules={[
                    {
                      required: true,
                      message: '必填项！',
                    },
                  ]}
                  label="物资名称"
                  name="mat_name"
                >
                  <Input placeholder={matInfo.mat_name} />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  rules={[
                    {
                      required: true,
                      message: '必填项！',
                    },
                  ]}
                  label="物资编码"
                  name="mat_code"
                >
                  <Input addonBefore={matInfo.typeCode} placeholder="两位一级" maxLength="2" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col span={12}>
                <Form.Item
                  rules={[
                    {
                      required: true,
                      message: '必填项！',
                    },
                  ]}
                  label="物资型号"
                  name="mat_size"
                >
                  <Input placeholder={matInfo.mat_size} />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  rules={[
                    {
                      required: true,
                      message: '必填项！',
                    },
                  ]}
                  label="添加时间"
                  name="add_date"
                >
                  <DatePicker disabled showTime format="YYYY/MM/DD HH:mm:ss" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col span={12}>
                <Form.Item
                  rules={[
                    {
                      required: true,
                      message: '必填项！',
                    },
                  ]}
                  label="物资单位"
                  name="mat_unit"
                >
                  <Input placeholder={matInfo.mat_unit} />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  rules={[
                    {
                      required: true,
                      message: '必填项！',
                    },
                  ]}
                  label="修改时间"
                  name="add_date"
                >
                  <DatePicker disabled showTime format="YYYY/MM/DD HH:mm:ss" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col span={12}>
                <Form.Item
                  rules={[
                    {
                      required: true,
                      message: '必填项！',
                    },
                  ]}
                  label="物资单价"
                  name="mat_price"
                >
                  <Input placeholder={matInfo.mat_price} />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  rules={[
                    {
                      required: true,
                      message: '必填项！',
                    },
                  ]}
                  label="物资描述"
                  name="mat_desc"
                >
                  <TextArea rows={2} placeholder={matInfo.mat_desc} />
                </Form.Item>
              </Col>
            </Row>
            <Form.Item>
              <Space size="large">
                <Button type="danger" htmlType="submit" style={{ width: '10vw' }}>
                  确认
                </Button>
                <Button
                  type="primary"
                  htmlType="reset"
                  style={{ width: '10vw' }}
                  onClick={myCancel}
                >
                  取消
                </Button>
              </Space>
            </Form.Item>
          </Form>
        </Modal>
      </Space>
    </PageContainer>
  );
};
