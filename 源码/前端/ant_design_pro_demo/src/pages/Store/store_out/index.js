import { PageContainer } from '@ant-design/pro-layout';
import { Table, Button, Input, Space, Card, Divider, Row, Col, Form, Modal } from 'antd';
import { Link } from 'umi';
import './index.less';
const { Search } = Input;
import { getStoreOuts } from '@/services/store';
import React, { useState, useEffect, useRef } from 'react';

export default (props) => {
  //form表单实例
  const [myform] = Form.useForm();
  //控制 弹窗
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [mats, setMats] = useState([]);
  const [matsOld, setMatsOld] = useState([]);

  //库存数据
  const [storeRecOld, setStoreRecOld] = useState([]);
  const [storeRec, setStoreRec] = useState([]);
  //数据加载状态
  const [loading, setLoading] = useState(false);
  //分页参数
  const [pagination, setPagination] = useState({
    currentPage: 1,
    pageSize: 10,
    total: 0,
  });
  const [pagination2, setPagination2] = useState({
    currentPage: 1,
    pageSize: 7,
    total: 0,
  });

  //加载数据（只加载一次）
  useEffect(() => {
    fetchData();
  }, []);

  //初次加载数据（查询100条）
  const fetchData = async () => {
    const res = await getStoreOuts({
      pagination: {
        currentPage: 1,
        pageSize: 100, //查询100条
        total: 0,
      },
    });
    setStoreRec(() => res.data.list);
    setStoreRecOld(() => res.data.list);
    setPagination(() => {
      return {
        // pageSize: res.data.pageSize,
        pageSize: 10, //一页显示10条
        currentPage: res.data.currentPage,
        total: res.data.total,
      };
    });
  };

  //重置按钮
  const resetBtn = () => {
    myform.resetFields();
    setStoreRec(() => storeRecOld);
  };

  //监听 所有输入框（搜索框 - 前端分页）
  const onChange = (allFields) => {
    let fieldsValue = myform.getFieldsValue(true); //各搜索框的 输入值
    let mydataList = []; //存放搜索结果
    let dataList_now = JSON.parse(JSON.stringify(storeRecOld)); //表格全部数据
    let fields_search = ['out_date', 'edit_user', 'house_name', 'out_code']; //要搜索的字段
    let isNotNull = false; //输入框是否为空
    for (let key in fieldsValue) {
      let search = fieldsValue[key]; //搜索框中输入的数据
      if (fields_search.indexOf(key) != -1 && search != '') {
        mydataList = [];
        isNotNull = true;
        mydataList.push(
          ...dataList_now.filter((data) =>
            (data[key] + '').toLowerCase().includes(search.toLowerCase()),
          ),
        );
        dataList_now = JSON.parse(JSON.stringify(mydataList)); //更新dataList_now
      }
    }
    if (mydataList.length == 0 && !isNotNull) {
      //输入框为空
      fetchData(); //重新加载数据
    } else {
      setStoreRec(() => mydataList);
      setPagination(() => {
        //设置分页
        return {
          currentPage: 1,
          pageSize: 10,
          total: 0,
        };
      });
    }
  };
  //监听 明细弹窗的搜索框
  const searchInput = (search) => {
    if (search.target) {
      // eslint-disable-next-line no-param-reassign
      search = search.target.value;
    }
    //搜索框
    if (search == '') {
      //变量search绑定输入框中输入的数据
      setMats(() => matsOld);
    }
    setMats(() =>
      matsOld.filter(
        (data) =>
          (data.mat_code + '').toLowerCase().includes(search.toLowerCase()) ||
          (data.mat_name + '').toLowerCase().includes(search.toLowerCase()) ||
          (data.mat_size + '').toLowerCase().includes(search.toLowerCase()) ||
          (data.mat_unit + '').toLowerCase().includes(search.toLowerCase()),
      ),
    );
  };

  //分页 数据获取（后端分页）
  const handleTableChange = (mypagination) => {
    let pg = {
      pagination: {
        pageSize: mypagination.pageSize,
        currentPage: mypagination.current,
      },
    };
    getStoreOuts(pg).then((res) => {
      setStoreRec(() => res.data.list);
      setStoreRecOld(() => res.data.list);
      setPagination(() => {
        return {
          pageSize: res.data.pageSize,
          currentPage: res.data.currentPage,
          total: res.data.total,
        };
      });
    });
  };
  //出库单 表格列名
  const columns = [
    {
      title: '仓库名称',
      dataIndex: 'house_name',
    },
    {
      title: '单号',
      dataIndex: 'out_code',
    },
    {
      title: '制单人',
      dataIndex: 'edit_user',
    },
    {
      title: '金额',
      dataIndex: 'out_money',
      render: (item) => `${parseFloat(item)}`,
    },
    {
      title: '物资数量',
      dataIndex: 'mats',
      render: (item, record) => {
        let num = 0;
        record.mats.forEach((item) => {
          num += parseFloat(item.out_num);
        });
        return num;
      },
    },
    {
      title: '时间',
      dataIndex: 'out_date',
    },
    {
      title: '操作',
      dataIndex: '操作',
      render: (text, record) => (
        <Space size="middle">
          <a onClick={handleShow.bind(this, record)}>查看明细</a>
        </Space>
      ),
    },
  ];

  //物资明细 列名
  const columns_detail = [
    {
      title: '物资名称',
      dataIndex: 'mat_name',
      render: (text, record) => <span style={{ color: '#1890ff' }}>{text}</span>,
    },
    {
      title: '物资编码',
      dataIndex: 'mat_code',
    },
    {
      title: '型号',
      dataIndex: 'mat_size',
    },
    {
      title: '数量',
      dataIndex: 'out_num',
      render: (text, record) => <span style={{ color: '#1890ff' }}>{parseFloat(text)}</span>,
    },
    {
      title: '单位',
      dataIndex: 'mat_unit',
    },
    {
      title: '单价',
      dataIndex: 'mat_price',
      render: (item) => `${parseFloat(item)}`,
    },
    {
      title: '金额',
      dataIndex: 'out_money',
      render: (text, record) => <span style={{ color: '#1890ff' }}>{parseFloat(text) + '元'}</span>,
    },
  ];
  //弹窗 确认按钮
  const handleOk = () => {
    setIsModalVisible(false);
    setMats(() => []);
    setMatsOld(() => []);
  };

  //弹窗 取消按钮
  const handleCancel = () => {
    setIsModalVisible(false);
    setMats(() => []);
    setMatsOld(() => []);
  };
  //查看明细
  const handleShow = (record) => {
    setIsModalVisible(true);
    setMats(() => record.mats);
    setMatsOld(() => record.mats);
  };
  return (
    <PageContainer>
      <Space direction="vertical" style={{ width: '100%' }} size="large">
        <Card bordered={false}>
          <Divider orientation="left" style={{ color: '#1890ff' }}>
            搜索
          </Divider>
          <Form
            form={myform}
            onFieldsChange={(_, allFields) => {
              onChange(allFields);
            }}
          >
            <Row gutter={16}>
              <Col className="gutter-row" span={5}>
                <Form.Item name="house_name">
                  <Search placeholder="仓库名称" allowClear />
                </Form.Item>
              </Col>
              <Col className="gutter-row" span={5}>
                <Form.Item name="out_code">
                  <Search placeholder="出库单" allowClear />
                </Form.Item>
              </Col>
              <Col className="gutter-row" span={5}>
                <Form.Item name="out_date">
                  <Search placeholder="时间" allowClear />
                </Form.Item>
              </Col>
              <Col className="gutter-row" span={5}>
                <Form.Item name="edit_user">
                  <Search placeholder="制单人" allowClear />
                </Form.Item>
              </Col>
              <Col className="gutter-row" span={4}>
                <Space size="large">
                  <Button type="primary" danger onClick={resetBtn}>
                    重置
                  </Button>
                  <Link to="/store/store_out/add">
                    <Button type="primary">新增</Button>
                  </Link>
                </Space>
              </Col>
            </Row>
          </Form>
        </Card>
        <Card bordered={false}>
          <Divider orientation="left" style={{ color: '#1890ff' }}>
            数据
          </Divider>
          <Table
            columns={columns}
            rowKey={(record) => record.out_code}
            dataSource={storeRec}
            pagination={pagination}
            loading={loading}
            onChange={handleTableChange}
          />
          <Modal
            centered
            title="物资明细"
            visible={isModalVisible}
            onOk={handleOk}
            onCancel={handleCancel}
            width={1200}
          >
            <Col span={6} style={{ marginBottom: '20px' }}>
              <Search
                placeholder="输入关键字搜索！"
                allowClear
                onSearch={searchInput}
                onInput={searchInput}
              />
            </Col>
            <Table
              columns={columns_detail}
              rowKey={(record) => record.mat_code}
              dataSource={mats}
              pagination={pagination2}
            />
          </Modal>
        </Card>
      </Space>
    </PageContainer>
  );
};
