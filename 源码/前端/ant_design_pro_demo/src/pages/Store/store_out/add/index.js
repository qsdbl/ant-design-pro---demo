import { PageContainer } from '@ant-design/pro-layout';
import { useState, useEffect } from 'react';
import styles from './index.less';
import {
  Form,
  Input,
  Button,
  Table,
  Card,
  Space,
  Row,
  Col,
  DatePicker,
  Popconfirm,
  notification,
  Select,
  Modal,
  Tree,
  message,
} from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import moment from 'moment';
import { getMatTree, getMats, postStoreOuts } from '@/services/store';
const Search = Input;
import { history } from 'umi';
import { ExclamationCircleOutlined } from '@ant-design/icons';
const { confirm } = Modal;

export default () => {
  //form表单实例
  const [myform] = Form.useForm();
  //分页参数
  const [pagination, setPagination] = useState({
    currentPage: 1,
    pageSize: 15,
  });
  const [pagination2, setPagination2] = useState({
    currentPage: 1,
    pageSize: 6,
  });
  //控制 弹窗
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalVisible2, setIsModalVisible2] = useState(false);

  let [selMats, setSelMats] = useState([]); //选择弹窗中的待选物资
  const [mats, setMats] = useState([]); //已选物资（显示在表单的表格中）
  const [editMatInfo, setEditMatInfo] = useState({}); //当前编辑的物资（编辑数量）
  //物资分类目录（树形结构）
  const [matTree, setMatTree] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      const res = await getMatTree();
      setMatTree(res.data);
      res.data.forEach((item) => {
        //加载物资
        //只加载电气类，演示用
        fetchMats(item);
      });
    };
    fetchData();
  }, []);

  const fetchMats = (data) => {
    getMats(data.type_name).then((resMat) => {
      setSelMats((data) => {
        return [...data, ...resMat.data];
      });
    });
    if (data.children && data.children.length > 0) {
      //若存在 子节点，一并查询
      data.children.forEach((item) => {
        fetchMats(item); //递归
      });
    }
  };

  //控件被选中回调
  const onSelect = (selectedKeys, info) => {
    setSelMats([]);
    fetchMats(info.node);
  };
  //重置按钮
  const myreset = () => {
    myform.resetFields();
    setMats([]);
  };
  //提交
  const onFinish = (values) => {
    let out_money = 0;
    mats.forEach((item) => {
      out_money += parseFloat(item.out_num * item.mat_price);
      //物资 入库金额
      item.out_money = parseFloat(item.out_num * item.mat_price);
    });
    //入库单创建时间
    values.out_date = moment(values.out_date).format('YYYY/MM/DD HH:mm:ss');
    //状态设置为“已提交”
    values.auditing = 1;
    //入库单 总金额
    values.out_money = out_money;
    values.house_id = 'jxstar-038-1';
    // 广州仓库 jxstar-038-1

    postStoreOuts({
      storeOut: values,
      storeMats: mats,
    })
      .then((res) => {
        if (res.success != 'true') {
          //创建失败
          notification.error({
            message: '创建失败！',
            description: res.message,
          });
        } else {
          confirm({
            title: '出库单创建成功！',
            icon: <ExclamationCircleOutlined />,
            content: '是否要返回出库单查询页面？',
            cancelText: '留在本页',
            okText: '返回',
            onOk() {
              history.push('/store/store_out');
            },
            onCancel() {
              myreset(); //重置表单
            },
          });
        }
      })
      .catch(() => {
        message.error('网络异常！');
      });
  };
  //表格 列名
  const columns = [
    {
      title: '物资名称',
      dataIndex: 'mat_name',
    },
    {
      title: '物资编码',
      dataIndex: 'mat_code',
    },
    {
      title: '物资分类',
      dataIndex: 'typeName',
    },
    {
      title: '型号',
      dataIndex: 'mat_size',
    },
    {
      title: '数量',
      dataIndex: 'out_num',
      render: (item) => `${parseFloat(item)}`,
    },
    {
      title: '单位',
      dataIndex: 'mat_unit',
    },
    {
      title: '单价',
      dataIndex: 'mat_price',
      render: (item) => `${parseFloat(item)}`,
    },
    {
      title: '备注',
      dataIndex: 'mat_desc',
    },
    {
      title: '操作',
      dataIndex: '操作',
      render: (text, record) => (
        <Space size="middle">
          <a onClick={handleEditNum.bind(this, record)}>编辑数量</a>
          <Popconfirm
            title="确认要删除该分类？"
            onConfirm={handleDelete.bind(this, record)}
            onCancel={cancel}
            okText="确认"
            cancelText="取消"
          >
            <a style={{ color: 'red' }}>删除</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];
  //取消删除
  const cancel = (record) => {
    message.error('已取消！');
  };

  //删除 按钮
  const handleDelete = (record) => {
    setMats((data) => {
      return data.filter((item) => item.mat_code != record.mat_code);
    });
    message.success(`已删除【${record.mat_name}】!`);
  };

  //待选物资表格 列名
  const selColumns = [
    {
      title: '物资名称',
      dataIndex: 'mat_name',
    },
    {
      title: '物资编码',
      dataIndex: 'mat_code',
    },
    {
      title: '物资分类',
      dataIndex: 'typeName',
    },
    {
      title: '型号',
      dataIndex: 'mat_size',
    },
    {
      title: '单位',
      dataIndex: 'mat_unit',
    },
    {
      title: '单价',
      dataIndex: 'mat_price',
      render: (item) => `${parseFloat(item)}`,
    },
    {
      title: '备注',
      dataIndex: 'mat_desc',
    },
    {
      title: '操作',
      dataIndex: '操作',
      render: (text, record) => (
        <Space size="middle">
          <a onClick={handleSel.bind(this, record)}>选择</a>
        </Space>
      ),
    },
  ];

  //选择物资
  const handleSel = (record) => {
    let mats_old = JSON.parse(JSON.stringify(mats));
    let isHave = false; //是否已有该物资
    mats_old.forEach((item) => {
      if (item.mat_code == record.mat_code) {
        isHave = true; //已有该物资
        item.out_num++; //数量加1
      }
    });
    if (!isHave) {
      //还未有该物资
      setMats(() => {
        record.out_num = 1;
        mats_old.push(record);
        return mats_old;
      });
    } else {
      //已有该物资
      setMats(() => {
        return mats_old;
      });
    }
    setIsModalVisible(false);
  };
  //打开添加/选择物资窗口
  const add = () => {
    setIsModalVisible(true);
  };
  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  //编辑物资数量 弹窗：
  const handleEditNum = (record) => {
    // console.log(record);
    setEditMatInfo(() => record);
    setIsModalVisible2(true);
  };
  const myCancel = () => {
    setIsModalVisible2(false);
  };
  //提交
  const onFinish2 = (values) => {
    // console.log(values);
    let mat_code = editMatInfo.mat_code;
    let out_num = values.out_num;
    if (out_num > 0) {
      setMats((data) => {
        data.forEach((item) => {
          if (item.mat_code == mat_code) {
            item.out_num = out_num;
          }
        });
        return data;
      });
      setIsModalVisible2(false);
    } else {
      message.error('数量必须大于0！');
    }
  };
  return (
    <PageContainer>
      <Form
        form={myform}
        name="basic"
        initialValues={{
          pur_user: 'Serati Ma',
          out_date: moment(new Date()),
          out_desc: '',
          house_name: '广州仓库',
        }}
        //提交表单且数据验证成功后
        onFinish={onFinish}
        autoComplete="off"
      >
        <Space direction="vertical" style={{ width: '100%' }} size="large">
          <Card title="出库单" bordered={false}>
            <Row>
              <Col span={7}>
                <Form.Item
                  label="仓库"
                  name="house_name"
                  rules={[
                    {
                      required: true,
                      message: '请选择仓库！',
                    },
                  ]}
                >
                  <Select disabled>
                    <Select.Option value="上海仓库">上海仓库</Select.Option>
                    <Select.Option value="北京仓库">北京仓库</Select.Option>
                    <Select.Option value="华强北仓库">华强北仓库</Select.Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col span={7} offset={1}>
                <Form.Item
                  label="仓管员"
                  name="edit_user"
                  rules={[
                    {
                      required: true,
                      message: '请选择仓管员！',
                    },
                  ]}
                >
                  <Select>
                    <Select.Option value="admin">admin</Select.Option>
                    <Select.Option value="小黑">小黑</Select.Option>
                    <Select.Option value="李华">李华</Select.Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col span={7} offset={1}>
                <Form.Item label="制单人" name="pur_user">
                  <Input disabled />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={7}>
                <Form.Item
                  label="送货人"
                  name="send_user"
                  rules={[
                    {
                      required: true,
                      message: '请选择送货人！',
                    },
                  ]}
                >
                  <Select>
                    <Select.Option value="admin">admin</Select.Option>
                    <Select.Option value="小黑">小黑</Select.Option>
                    <Select.Option value="李华">李华</Select.Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col span={7} offset={1}>
                <Form.Item label="制单时间" name="out_date">
                  <DatePicker disabled showTime format="YYYY/MM/DD HH:mm:ss" />
                </Form.Item>
              </Col>
              <Col span={7} offset={1}>
                <Form.Item label="备注" name="out_desc">
                  <Input />
                </Form.Item>
              </Col>
            </Row>
          </Card>

          <Card title="出库明细" bordered={false}>
            <Form.Item label="" name="mats" fields={mats}>
              <Table
                columns={columns}
                rowKey={(record) => record.name}
                dataSource={mats}
                pagination={pagination}
              />
              <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                选择物资
              </Button>

              <Modal
                title="编辑数量"
                visible={isModalVisible2}
                width={500}
                centered
                footer={null}
                destroyOnClose
                onCancel={myCancel}
              >
                <Form
                  preserve={false}
                  name="basic"
                  initialValues={{
                    out_num: editMatInfo.out_num,
                  }}
                  //提交表单且数据验证成功后
                  onFinish={onFinish2}
                  autoComplete="off"
                >
                  <Form.Item
                    rules={[
                      {
                        required: true,
                        message: '必填项！',
                      },
                    ]}
                    label="物资数量"
                    name="out_num"
                  >
                    <Input placeholder={editMatInfo.out_num} type="number" />
                  </Form.Item>
                  <Form.Item>
                    <Space size="large">
                      <Button type="danger" htmlType="submit" style={{ width: '10vw' }}>
                        确认
                      </Button>
                      <Button
                        type="primary"
                        htmlType="reset"
                        style={{ width: '10vw' }}
                        onClick={myCancel}
                      >
                        取消
                      </Button>
                    </Space>
                  </Form.Item>
                </Form>
              </Modal>
              <Modal
                title="选择物资"
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
                width={1200}
              >
                <Row gutter={16}>
                  <Col span={4}>
                    <Search style={{ marginBottom: 8 }} placeholder="输入关键字搜索！" />
                    <Tree
                      blockNode
                      treeData={matTree}
                      fieldNames={{ title: 'type_name', key: 'type_id', children: 'children' }}
                      onSelect={onSelect}
                    />
                  </Col>
                  <Col span={20}>
                    <Table
                      columns={selColumns}
                      rowKey={(record) => record.name}
                      dataSource={selMats}
                      pagination={pagination2}
                    />
                  </Col>
                </Row>
              </Modal>
            </Form.Item>
          </Card>
          <Card bordered={false} style={{ height: '100px' }} className={styles.btn_box}>
            <Space size="large">
              <Button
                type="danger"
                htmlType="reset"
                style={{ width: '10vw', marginRight: '50px' }}
                onClick={myreset}
              >
                重置
              </Button>
              <Button type="primary" htmlType="submit" style={{ width: '10vw' }}>
                提交
              </Button>
            </Space>
          </Card>
        </Space>
      </Form>
    </PageContainer>
  );
};
