import { PageContainer } from '@ant-design/pro-layout';
import { Table, Input, Space, Card, Divider, Row, Col, Form } from 'antd';
const { TextArea, Search } = Input;
import React, { useState } from 'react';

export default (props) => {
  //数据
  const [storeIn, setStoreIn] = useState(() => {
    return JSON.parse(sessionStorage.getItem('store_in_show'));
  });
  const [indetMats, setIndetMats] = useState(() => {
    return JSON.parse(sessionStorage.getItem('store_in_show')).mats;
  });
  const [indetMatsOld, setIndetMatsOld] = useState(() => {
    return JSON.parse(sessionStorage.getItem('store_in_show')).mats;
  });
  //数据加载状态
  const [loading, setLoading] = useState(false);
  //分页参数
  const [pagination, setPagination] = useState({
    currentPage: 1,
    pageSize: 7,
    total: 0,
  });

  const searchInput = (search) => {
    if (search.target) {
      // eslint-disable-next-line no-param-reassign
      search = search.target.value;
    }
    //搜索框
    if (search == '') {
      //变量search绑定输入框中输入的数据
      setIndetMats(indetMatsOld);
    }
    setIndetMats(
      indetMatsOld.filter(
        (indetMats) =>
          (indetMats.mat_code + '').toLowerCase().includes(search.toLowerCase()) ||
          (indetMats.mat_name + '').toLowerCase().includes(search.toLowerCase()) ||
          (indetMats.mat_size + '').toLowerCase().includes(search.toLowerCase()) ||
          (indetMats.mat_unit + '').toLowerCase().includes(search.toLowerCase()),
      ),
    );
  };
  //物资明细 列名
  const columns = [
    // 物料名	数量	单位	单价
    {
      title: '物资名称',
      dataIndex: 'mat_name',
      render: (text, record) => <span style={{ color: '#1890ff' }}>{text}</span>,
    },
    {
      title: '物资编码',
      dataIndex: 'mat_code',
    },
    {
      title: '型号',
      dataIndex: 'mat_size',
    },
    {
      title: '数量',
      dataIndex: 'in_num',
      render: (text, record) => <span style={{ color: '#1890ff' }}>{parseFloat(text)}</span>,
    },
    {
      title: '单位',
      dataIndex: 'mat_unit',
    },
    {
      title: '单价',
      dataIndex: 'mat_price',
      render: (item) => `${parseFloat(item)}`,
    },
    {
      title: '金额',
      dataIndex: 'in_money',
      render: (text, record) => <span style={{ color: '#1890ff' }}>{parseFloat(text) + '元'}</span>,
    },
  ];

  return (
    <PageContainer>
      <Space direction="vertical" style={{ width: '100%' }} size="large">
        <Card bordered={false}>
          <Divider orientation="left" style={{ color: '#1890ff' }}>
            入库单
          </Divider>
          <Form
            initialValues={{
              house_name: storeIn.house_name,
              in_code: storeIn.in_code,
              edit_user: storeIn.edit_user,
              send_user: storeIn.send_user,
              in_date: storeIn.in_date,
              in_money: parseFloat(storeIn.in_money),
              in_desc: storeIn.in_desc,
            }}
          >
            <Row gutter={16}>
              <Col span={7}>
                <Form.Item label="仓库名称" name="house_name">
                  <Input disabled />
                </Form.Item>
              </Col>
              <Col span={7} offset="1">
                <Form.Item label="入库单号" name="in_code">
                  <Input disabled />
                </Form.Item>
              </Col>
              <Col span={7} offset="1">
                <Form.Item label="入库时间" name="in_date">
                  <Input disabled />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={7}>
                <Form.Item label="仓管人员" name="edit_user">
                  <Input disabled />
                </Form.Item>
              </Col>
              <Col span={7} offset="1">
                <Form.Item label="送货人员" name="send_user">
                  <Input disabled />
                </Form.Item>
              </Col>
              <Col span={7} offset="1">
                <Form.Item label="入库金额" name="in_money">
                  <Input disabled />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="备注" name="in_desc">
                  <TextArea rows={3} disabled />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Card>
        <Card bordered={false}>
          <Divider orientation="left" style={{ color: '#1890ff' }}>
            物资明细
          </Divider>
          <Row gutter={16} style={{ marginBottom: '20px' }}>
            <Col span={6}>
              <Search
                placeholder="输入关键字搜索！"
                onSearch={searchInput}
                onInput={searchInput}
                allowClear
              />
            </Col>
          </Row>
          <Table
            columns={columns}
            rowKey={(record) => record.in_id}
            dataSource={indetMats}
            pagination={pagination}
            loading={loading}
          />
        </Card>
      </Space>
    </PageContainer>
  );
};
