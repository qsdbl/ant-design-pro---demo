import { PageContainer } from '@ant-design/pro-layout';
import { Table, Button, Input, Space, Card, Divider, Row, Col, Form } from 'antd';
import './index.less';
const { Search } = Input;
import { getStoreRec } from '@/services/store';
import React, { useState, useEffect } from 'react';

export default (props) => {
  //form表单实例
  const [myform] = Form.useForm();
  //库存数据
  const [storeRecOld, setStoreRecOld] = useState([]);
  const [storeRec, setStoreRec] = useState([]);
  //数据加载状态
  const [loading, setLoading] = useState(false);
  //分页参数
  const [pagination, setPagination] = useState({
    currentPage: 1,
    pageSize: 10,
    total: 0,
  });

  //加载数据（只加载一次）
  useEffect(() => {
    fetchData();
  }, []);

  //初次加载数据（查询100条）
  const fetchData = async () => {
    const res = await getStoreRec({
      pagination: {
        currentPage: 1,
        pageSize: 100, //查询100条
        total: 0,
      },
    });
    setStoreRec(() => res.data.list);
    setStoreRecOld(() => res.data.list);
    setPagination(() => {
      return {
        pageSize: 10, //一页显示10条
        currentPage: res.data.currentPage,
        total: res.data.total,
      };
    });
  };

  //分页 数据获取
  const handleTableChange = (mypagination) => {
    let pg = {
      pagination: {
        pageSize: mypagination.pageSize,
        currentPage: mypagination.current,
      },
    };
    getStoreRec(pg).then((res) => {
      setStoreRec(res.data.list);
      setStoreRecOld(res.data.list);
      setPagination({
        pageSize: res.data.pageSize,
        currentPage: res.data.currentPage,
        total: res.data.total,
      });
    });
  };
  //表格 列名
  const columns = [
    {
      title: '仓库名称',
      dataIndex: 'house_name',
    },
    {
      title: '物资名称',
      dataIndex: 'mat_name',
    },
    {
      title: '物资编码',
      dataIndex: 'mat_code',
    },
    {
      title: '物资型号',
      dataIndex: 'mat_size',
    },
    {
      title: '库存数量',
      dataIndex: 'store_num',
      render: (item) => `${parseFloat(item)}`,
    },
    {
      title: '单位',
      dataIndex: 'mat_unit',
    },
    {
      title: '库存单价',
      dataIndex: 'store_price',
      render: (item) => `${parseFloat(item)}`,
    },
    {
      title: '库存金额',
      dataIndex: 'store_money',
      render: (item) => `${parseFloat(item)}`,
    },
    {
      title: '最近入库单号',
      dataIndex: 'last_code',
    },
    {
      title: '最近入库时间',
      dataIndex: 'last_date',
    },
    // {
    //   title: '操作',
    //   dataIndex: '操作',
    //   render: () => (
    //     <Space size="middle">
    //       <a onClick={handleEdit}>编辑</a>
    //       <a onClick={handleDelete}>删除</a>
    //     </Space>
    //   ),
    // },
  ];

  //监听 所有输入框（搜索框 - 前端分页）
  const onChange = (allFields) => {
    let fieldsValue = myform.getFieldsValue(true); //各搜索框的 输入值
    let mydataList = []; //存放搜索结果
    let dataList_now = JSON.parse(JSON.stringify(storeRecOld)); //表格全部数据
    let fields_search = ['mat_size', 'mat_code', 'mat_name', 'house_name']; //要搜索的字段
    let isNotNull = false; //输入框是否为空
    for (let key in fieldsValue) {
      let search = fieldsValue[key]; //搜索框中输入的数据
      if (fields_search.indexOf(key) != -1 && search != '') {
        mydataList = [];
        isNotNull = true;
        mydataList.push(
          ...dataList_now.filter((data) =>
            (data[key] + '').toLowerCase().includes(search.toLowerCase()),
          ),
        );
        dataList_now = JSON.parse(JSON.stringify(mydataList)); //更新dataList_now
      }
    }
    if (mydataList.length == 0 && !isNotNull) {
      //输入框为空
      fetchData(); //重新加载数据
    } else {
      setStoreRec(() => mydataList);
      setPagination(() => {
        //设置分页
        return {
          currentPage: 1,
          pageSize: 10,
          total: 0,
        };
      });
    }
  };
  //重置按钮
  const resetBtn = () => {
    myform.resetFields();
    setStoreRec(() => storeRecOld);
  };

  return (
    <PageContainer>
      <Space direction="vertical" style={{ width: '100%' }} size="large">
        <Card bordered={false}>
          <Divider orientation="left" style={{ color: '#1890ff' }}>
            搜索
          </Divider>
          <Form
            form={myform}
            onFieldsChange={(_, allFields) => {
              onChange(allFields);
            }}
          >
            <Row gutter={16}>
              <Col className="gutter-row" span={5}>
                <Form.Item name="house_name">
                  <Search placeholder="仓库名称" allowClear />
                </Form.Item>
              </Col>
              <Col className="gutter-row" span={5}>
                <Form.Item name="mat_name">
                  <Search placeholder="物资名称" allowClear />
                </Form.Item>
              </Col>
              <Col className="gutter-row" span={5}>
                <Form.Item name="mat_size">
                  <Search placeholder="物资型号" allowClear />
                </Form.Item>
              </Col>
              <Col className="gutter-row" span={5}>
                <Form.Item name="mat_code">
                  <Search placeholder="物资编码" allowClear />
                </Form.Item>
              </Col>
              <Col className="gutter-row" span={4}>
                <Button block type="primary" danger onClick={resetBtn}>
                  重置
                </Button>
              </Col>
            </Row>
          </Form>
        </Card>
        <Card bordered={false}>
          <Divider orientation="left" style={{ color: '#1890ff' }}>
            数据
          </Divider>
          <Table
            columns={columns}
            rowKey={(record) => record.store_id}
            dataSource={storeRec}
            pagination={pagination}
            loading={loading}
            onChange={handleTableChange}
          />
        </Card>
      </Space>
    </PageContainer>
  );
};
