import { PageContainer } from '@ant-design/pro-layout';
import {
  Table,
  Button,
  Input,
  Space,
  Card,
  Row,
  Col,
  Modal,
  Form,
  Popconfirm,
  message,
  Tree,
  AutoComplete,
  notification,
} from 'antd';
import { ExclamationCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import './index.less';
const { Search } = Input;
import React, { useState, useEffect } from 'react';
import { getMatTree, updateMT, addMT, deleteMT } from '@/services/store';
const { confirm } = Modal;

export default (props) => {
  //默认展开的节点
  const [expandedKeys, setExpandedKeys] = useState([]);
  //物资分类（树形）
  const [matTree, setMatTree] = useState([]);
  const [matTreeOld, setMatTreeOld] = useState([]);
  //控制 弹窗
  const [isModalVisible, setIsModalVisible] = useState(false);
  //控制 新增弹窗
  const [isModalVisible2, setIsModalVisible2] = useState(false);
  //弹窗数据
  const [typeInfo, setTypeInfo] = useState({});
  //弹窗数据（新增节点）
  const [typeInfo_add, setTypeInfoAdd] = useState({});

  //库存数据
  const [mType, setMType] = useState([]);
  //数据加载状态
  const [loading, setLoading] = useState(false);

  //初次获取数据
  useEffect(() => {
    loadData();
  }, []);

  //加载页面数据
  const loadData = () => {
    //重新加载数据
    getMatTree().then((res) => {
      setMatTree(res.data);
      setMatTreeOld(res.data);
      //默认展示第一个分类的数据
      onSelect('', {
        node: res.data[0],
      });
    });
  };

  //过滤 树
  const searchInput = (search) => {
    if (search.target) {
      // eslint-disable-next-line no-param-reassign
      search = search.target.value;
    }
    //搜索框
    if (search == '') {
      //变量search绑定输入框中输入的数据
      setMatTree(matTreeOld);
    }
    setMatTree(matTreeOld.filter((mtree) => filterNode(search, mtree)));
  };
  const filterNode = (value, data) => {
    //tree过滤（搜索输入框）
    //value - filterText变量的值，data - 节点数据（所有节点都会被过滤一遍）
    if (!value) return true; //搜索输入框为空时（没有输入数据的情况），返回全部的数据
    return data.type_name.indexOf(value) !== -1; //过滤 分类名称，返回分类名称中包含value值的 节点
  };

  //表格 列名
  const columns = [
    {
      title: '分类名称',
      dataIndex: 'type_name',
      render: (text, record) => (
        <span style={record.isHeader ? { color: '#1890ff' } : {}}>{text}</span>
      ),
    },
    {
      title: '分类编码',
      dataIndex: 'type_code',
      render: (text, record) => (
        <span style={record.isHeader ? { color: '#1890ff' } : {}}>{text}</span>
      ),
    },
    {
      title: '分类描述',
      dataIndex: 'type_desc',
      render: (text, record) => (
        <span style={record.isHeader ? { color: '#1890ff' } : {}}>{text}</span>
      ),
    },
    {
      title: '操作',
      dataIndex: '操作',
      render: (text, record) => (
        <Space size="middle">
          <a onClick={handleEdit.bind(this, record)}>编辑</a>
          <Popconfirm
            title="确认要删除该分类？"
            onConfirm={handleDelete.bind(this, record)}
            onCancel={cancel}
            okText="确认"
            cancelText="取消"
          >
            <a style={{ color: 'red' }}>删除</a>
          </Popconfirm>
          {/* {record.isHeader ? ( */}
          <a onClick={handleAdd.bind(this, record)} style={{ color: '#fd9826' }}>
            新增
          </a>
          {/* ) : (
            ''
          )} */}
        </Space>
      ),
    },
  ];
  //新增 按钮
  const handleAdd = (record) => {
    setIsModalVisible2(true);
    setTypeInfoAdd(record);
  };
  //编辑 按钮
  const handleEdit = (record) => {
    setIsModalVisible(true);
    setTypeInfo(() => record);
  };

  //删除 按钮
  const handleDelete = (record) => {
    deleteMT(record.type_code)
      .then((res) => {
        //重新加载数据
        loadData();
        if (res.success != 'true') {
          //删除失败
          notification.error({
            message: '删除失败！',
            description: '节点"' + record.type_name + '"删除失败！' + res.message,
          });
        } else {
          //删除成功
          notification.success({
            message: '删除成功！',
            description: '节点"' + record.type_name + '"，删除成功！',
          });
        }
      })
      .catch(() => {
        message.error('网络异常！');
      });
  };
  //取消删除
  const cancel = (record) => {
    message.error('已取消！');
  };

  //编辑弹窗：
  //提交
  const onFinish = (values) => {
    // console.log('Success:', values);
    values.type_id = typeInfo.type_id;
    values.type_code =
      (typeInfo.type_code + '').substring(0, (typeInfo.type_code + '').length - 2) +
      values.type_code;
    var new_val = ''; //更改的字段 的新数据
    var old_val = ''; //更改的字段 的旧数据
    var name_now = typeInfo.type_name; //节点名字（分类名称）
    if (typeInfo.type_code != values.type_code) {
      //分类编码
      new_val += values.type_code + '、';
      old_val += typeInfo.type_code + '、';
    }
    if (typeInfo.type_name != values.type_name) {
      //分类名称
      new_val += values.type_name + '、';
      old_val += typeInfo.type_name + '、';
      name_now = values.type_name; //若节点名字 发生修改 则更新name_now
    }
    if (typeInfo.type_desc != values.type_desc) {
      //分类描述
      new_val += values.type_desc;
      old_val += typeInfo.type_desc;
    }
    if (new_val.charAt(new_val.length - 1) == '、') {
      //去除后边的 、
      new_val = new_val.substring(0, new_val.length - 1);
      old_val = old_val.substring(0, old_val.length - 1);
    }

    var params = {
      type_code_new: typeInfo.type_code,
      data: values,
    };
    updateMT(params)
      .then((res) => {
        //重新加载数据
        loadData();
        if (res.success != 'true') {
          //编辑失败
          notification.error({
            message: '编辑失败！',
            description: '分类名称：' + name_now + '；' + res.message,
          });
        } else {
          //修改成功
          notification.success({
            message: '修改成功！',
            description: '分类名称：' + name_now + '；新：' + new_val + '；旧：' + old_val,
          });
          setTypeInfo({});
          setIsModalVisible(false);
        }
      })
      .catch(() => {
        message.error('网络异常！');
      });
  };

  //新增弹窗：
  //提交
  const onFinishAdd = (values) => {
    values.type_code = '' + typeInfo_add.type_code + values.type_code; //拼接 分类编码（父节点分类编码 固定不能修改，两位一级故新增节点时只能编辑两位编码，然后拼接父节点分类编码）
    var name = values.type_name; //临时保存 用在通知中
    var parent_name = ''; //临时保存 用在通知中
    if (typeInfo_add.type_name) {
      parent_name = '；父节点：' + typeInfo_add.type_name;
    } else {
      parent_name = '';
    }
    addMT(values)
      .then((res) => {
        //重新加载数据
        loadData();
        if (res.success != 'true') {
          //创建失败
          notification.error({
            message: '创建失败！',
            description: res.message,
          });
        } else {
          //添加成功
          notification.success({
            message: '添加成功！',
            description: '分类名称：' + name + parent_name,
          });
          setTypeInfoAdd({});
          setIsModalVisible2(false);
        }
      })
      .catch(() => {
        message.error('网络异常！');
      });
  };

  //取消
  const myCancel = () => {
    //编辑弹窗
    setIsModalVisible(false);
    //新增弹窗
    setIsModalVisible2(false);
  };
  //控件被选中回调
  const onSelect = (selectedKeys, info) => {
    let mydata = JSON.parse(JSON.stringify(info.node.children));
    let node_now = JSON.parse(JSON.stringify(info.node));
    delete node_now.children;
    node_now.isHeader = true;
    mydata.unshift(node_now);
    setMType(deleteChildren(mydata));
  };
  //若属性children为空，则去掉
  const deleteChildren = (objs) => {
    objs.forEach((obj) => {
      if (obj.children) {
        if (obj.children.length == 0) {
          delete obj.children;
        } else {
          deleteChildren(obj.children);
        }
      }
    });
    return objs;
  };
  //拖动节点
  const onDrop = (info) => {
    // console.log('info.dragNode');
    // console.log(info.dragNode);
    //info.dragNode 拖曳的节点
    //info.node 目的节点（放置位置在其下边）
    //info.dropToGap true，平级。false，子元素
    var code_old = info.dragNode.type_code; //被拖拽节点的 分类编码，后边将求出父节点分类编码
    var code_now = info.node.type_code; //结束拖拽时最后进入的节点的 分类编码，后边将求出父节点分类编码
    var type_code_new = ''; //新 分类编码（拖拽后生成的分类编码）
    var type_code_end = ''; //新 分类编码，后半部分（两位一级，故长度为1到2）
    if (code_now.length > 2) {
      if (code_now.length % 2 == 0) {
        //两位一级，去除父节点的分类编码后，将会剩下当前节点的1到2位的实际编码。故下边通过模运算确定父节点的分类编码，保存在code_now
        code_now = code_now.substr(0, code_now.length - 2);
      } else {
        code_now = code_now.substr(0, code_now.length - 1);
      }
    }
    if (code_old.length > 2) {
      if (code_old.length % 2 == 0) {
        type_code_end = code_old.substr(code_old.length - 2, code_old.length - 1);
        code_old = code_old.substr(0, code_old.length - 2);
      } else {
        type_code_end = code_old.substr(code_old.length - 1, code_old.length - 1);
        code_old = code_old.substr(0, code_old.length - 1);
      }
    }
    if (info.dropToGap == false) {
      //被拖拽节点的放置位置为inner， 将成为子节点
      //新 分类编码 = 结束拖拽时最后进入的节点的 分类编码 + 原编码的后半部分
      type_code_new = info.node.type_code + type_code_end;
    } else {
      //新 分类编码 = 结束拖拽时最后进入的节点的 父节点的分类编码 + 原编码的后半部分
      type_code_new = code_now + type_code_end;
    }
    if (code_now == code_old && info.dropToGap != false) {
      //同级之间移动 且 被拖拽节点的放置位置不是inner（成为子节点）
      // this.tree.data = JSON.parse(JSON.stringify(this.tree.data_old)); //还原 tree数据
      confirm({
        title: '提示',
        icon: <ExclamationCircleOutlined />,
        content: '同级之间无需移动！',
      });
    } else {
      //不同级之间移动 或 成为子节点
      if (info.dragNode.type_code.length == 2) {
        confirm({
          title: '警告',
          icon: <CloseCircleOutlined />,
          content: '不能移动顶层节点！',
        });
      } else {
        var msg = '';
        if (info.dropToGap) {
          //平级
          msg = `【${info.dragNode.type_name}】节点将与【${info.node.type_name}】节点同级！`;
        } else {
          //作为子节点
          msg = `【${info.dragNode.type_name}】节点将作为【${info.node.type_name}】节点的子节点！`;
        }
        confirm({
          title: '确定要移动该项目吗？',
          icon: <ExclamationCircleOutlined />,
          content: msg,
          onOk() {
            //点击 确认 后
            var data = info.dragNode; //被拖拽节点的 信息
            data.type_code = type_code_new; //更新节点分类编码（更新为拖拽后生成的分类编码）
            updateMT({
              data: data,
              type_code_new: type_code_new,
            })
              .then((res) => {
                //Ajax修改节点 分类编码
                getMatTree().then((res) => {
                  setMatTree(res.data);
                });
                if (res.success != 'true') {
                  //移动失败
                  message.error('移动失败！' + res.message);
                } else {
                  //移动成功
                  message.success('移动成功！');
                }
              })
              .catch(() => {
                message.error('网络异常！');
              });
          },
          onCancel() {
            //点击 取消 后
            message.error('移动操作已撤销！');
          },
        });
      }
    }
  };
  //新增根节点
  const addNode = () => {
    setIsModalVisible2(true);
    setTypeInfoAdd({
      isRoot: true,
      type_code: '',
      type_name: '',
      type_desc: '',
    });
  };

  return (
    <PageContainer>
      <Space direction="vertical" style={{ width: '100%' }} size="large">
        <Card bordered={false}>
          <Row gutter={16}>
            <Col span={6}>
              <Space style={{ marginBottom: 8 }}>
                <Search
                  placeholder="输入关键字搜索！"
                  onSearch={searchInput}
                  onInput={searchInput}
                  allowClear
                />
                <Button type="primary" onClick={addNode}>
                  新增根节点
                </Button>
              </Space>
              <Tree
                showLine={{ showLeafIcon: false }}
                blockNode
                draggable
                onDrop={onDrop}
                treeData={matTree}
                fieldNames={{ title: 'type_name', key: 'type_code', children: 'children' }}
                onSelect={onSelect}
                defaultExpandedKeys={expandedKeys}
              />
            </Col>
            <Col span={18}>
              <Table
                columns={columns}
                rowKey={(record) => record.type_code}
                dataSource={mType}
                pagination={false}
                expandable={{
                  rowExpandable: (record) => !record.isHeader,
                }}
              />
            </Col>
          </Row>
        </Card>
        <Modal
          title="编辑"
          centered
          visible={isModalVisible}
          width={700}
          footer={null}
          destroyOnClose
          onCancel={myCancel}
        >
          <Form
            preserve={false}
            name="basic"
            initialValues={{
              type_name: typeInfo.type_name,
              type_code: (typeInfo.type_code + '').substring((typeInfo.type_code + '').length - 2),
              type_desc: typeInfo.type_desc,
            }}
            //提交表单且数据验证成功后
            onFinish={onFinish}
            autoComplete="off"
          >
            <Form.Item
              label="分类编码"
              name="type_code"
              rules={[
                {
                  required: true,
                  message: '请设置编码！两位一级',
                },
              ]}
            >
              <Input
                placeholder={(typeInfo.type_code + '').substring(
                  (typeInfo.type_code + '').length - 2,
                )}
                addonBefore={(typeInfo.type_code + '').substring(
                  0,
                  (typeInfo.type_code + '').length - 2,
                )}
                maxLength="2"
              />
            </Form.Item>
            <Form.Item
              label="分类名称"
              name="type_name"
              rules={[
                {
                  required: true,
                  message: '请填写分类名称！',
                },
              ]}
            >
              <AutoComplete
                options={[{ label: typeInfo.type_name, value: typeInfo.type_name }]}
                placeholder={typeInfo.type_name}
              />
            </Form.Item>
            <Form.Item label="分类描述" name="type_desc">
              <AutoComplete
                options={[{ label: typeInfo.type_desc, value: typeInfo.type_desc }]}
                placeholder={typeInfo.type_desc}
              />
            </Form.Item>
            <Form.Item>
              <Space size="large">
                <Button type="danger" htmlType="submit" style={{ width: '10vw' }}>
                  确认
                </Button>
                <Button
                  type="primary"
                  htmlType="reset"
                  style={{ width: '10vw' }}
                  onClick={myCancel}
                >
                  取消
                </Button>
              </Space>
            </Form.Item>
          </Form>
        </Modal>
        <Modal
          title="新增节点"
          centered
          visible={isModalVisible2}
          width={700}
          footer={null}
          destroyOnClose
          onCancel={myCancel}
        >
          <Form
            preserve={false}
            name="basic"
            initialValues={{
              type_code: '',
              type_name: '',
              type_desc: '',
            }}
            //提交表单且数据验证成功后
            onFinish={onFinishAdd}
            autoComplete="off"
          >
            <Form.Item
              label="分类编码"
              name="type_code"
              rules={[
                {
                  required: true,
                  message: '请设置编码！',
                },
              ]}
            >
              <Input addonBefore={typeInfo_add.type_code} placeholder="两位一级" maxLength="2" />
            </Form.Item>
            <Form.Item
              label="分类名称"
              name="type_name"
              rules={[
                {
                  required: true,
                  message: '请填写名称！',
                },
              ]}
            >
              <Input
                placeholder={
                  typeInfo_add.isRoot
                    ? '将作为根节点！'
                    : `作为【${typeInfo_add.type_name}】节点的子节点`
                }
              />
            </Form.Item>
            <Form.Item label="分类描述" name="type_desc">
              <Input />
            </Form.Item>
            <Form.Item label="上级节点" name="type_parent">
              <Input disabled placeholder={typeInfo_add.type_name} />
            </Form.Item>
            <Form.Item>
              <Space size="large">
                <Button type="danger" htmlType="submit" style={{ width: '10vw' }}>
                  确认
                </Button>
                <Button
                  type="primary"
                  htmlType="reset"
                  style={{ width: '10vw' }}
                  onClick={myCancel}
                >
                  取消
                </Button>
              </Space>
            </Form.Item>
          </Form>
        </Modal>
      </Space>
    </PageContainer>
  );
};
