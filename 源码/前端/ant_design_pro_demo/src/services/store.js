import request from 'umi-request';

//库存
export async function getStoreRec(params) {
  return request.get('http://localhost:8090/nazox_demo/v1/storerecs', {
    params: {
      pageSize: params.pagination.pageSize,
      currentPage: params.pagination.currentPage,
    },
  });
}

//出库单：
//查询出库单
export async function getStoreOuts(params) {
  return request.get('http://localhost:8090/nazox_demo/v1/storeouts', {
    params: {
      pageSize: params.pagination.pageSize,
      currentPage: params.pagination.currentPage,
    },
  });
}
//创建出库单
export async function postStoreOuts(params) {
  return request.post('http://localhost:8090/nazox_demo/v1/storeout', {
    data: {
      storeOut: params.storeOut,
      storeMats: params.storeMats,
    },
  });
}

//入库单：
//查询入库单
export async function getStoreIns(params) {
  return request.get('http://localhost:8090/nazox_demo/v1/storeins', {
    params: {
      pageSize: params.pagination.pageSize,
      currentPage: params.pagination.currentPage,
    },
  });
}
//创建入库单
export async function postStoreIns(params) {
  return request.post('http://localhost:8090/nazox_demo/v1/storein', {
    data: {
      storeIn: params.storeIn,
      storeMats: params.storeMats,
    },
  });
}

//仓库目录：
//获取 仓库目录
export async function getHouses(params) {
  return request.get('http://localhost:8090/nazox_demo/v1/storehouses', {
    params: {
      pageSize: params.pagination.pageSize,
      currentPage: params.pagination.currentPage,
    },
  });
}

//删除 仓库目录
export async function delHouse(house_id) {
  return request.delete('http://localhost:8090/nazox_demo/v1/storehouse/' + house_id);
}

//修改 仓库目录
export async function updateHouse(params) {
  return request.patch('http://localhost:8090/nazox_demo/v1/storehouse/' + params.house_code, {
    data: params,
  });
}

//新增 仓库目录
export async function addHouse(params) {
  return request.post('http://localhost:8090/nazox_demo/v1/storehouse/' + params.house_code, {
    data: params,
  });
}

//物资分类：
//修改 物资分类
export async function updateMT(params) {
  return request.patch('http://localhost:8090/nazox_demo/v1/storemtype/' + params.type_code_new, {
    data: params.data,
  });
}

//新增 物资分类
export async function addMT(params) {
  return request.post('http://localhost:8090/nazox_demo/v1/storemtype/' + params.type_code, {
    data: params,
  });
}

//删除 物资分类
export async function deleteMT(type_code) {
  return request.delete('http://localhost:8090/nazox_demo/v1/storemtype/' + type_code);
}

//查询 物资分类（树形）
export async function getMatTree() {
  return request.get('http://localhost:8090/nazox_demo/v1/storemtypes');
}
//根据分类查找物资数据
export async function getMats(type_name) {
  return request.get('http://localhost:8090/nazox_demo/v1/storemats/' + type_name);
}

//物资目录：
//修改 物资目录
export async function updateMat(params) {
  return request.patch('http://localhost:8090/nazox_demo/v1/storemat/' + params.mat_code, {
    data: params,
  });
}

//新增 物资目录
export async function addMat(params) {
  return request.post('http://localhost:8090/nazox_demo/v1/storemat/' + params.mat_code, {
    data: params,
  });
}

//删除 物资目录
export async function deleteMat(mat_id) {
  return request.delete('http://localhost:8090/nazox_demo/v1/storemat/' + mat_id);
}
