# Ant Design Pro

基于Ant Design Pro开发的demo！


## 下载依赖
```bash
npm install
```

## 启动项目

```bash
npm start
```

## 构建项目

```bash
npm run build
```