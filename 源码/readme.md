## 说明

### 前端环境

node 14.17.1、npm 6.14.13

### 后端环境

JDK1.8、MySQL 5.5.53

### 数据库

数据库名：jxstar_cloud
用户名：root
密码：123456
数据库数据文件为“源码\后端\db\mysql_backup.sql”