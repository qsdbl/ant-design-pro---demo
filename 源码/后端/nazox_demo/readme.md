# nazox_demo

编译后的程序在target文件夹内（仅jar包其他文件不是）。使用下边的命令运行：

``` bash
# 启动项目(8090端口)
java -jar jar文件（jar后缀）
```



## 关于项目源码

- 在properties文件中修改MySQL数据库的连接参数。
  - 数据库地址为`localhost:3306/jxstar_cloud`
  - 默认的用户名为root
  - 默认的密码为123456
- 若要修改程序中的配置，需要修改后重新编译。项目编译：IDEA打开本项目，在侧栏中展开maven工具，分别使用clean，package命令完成打包。



