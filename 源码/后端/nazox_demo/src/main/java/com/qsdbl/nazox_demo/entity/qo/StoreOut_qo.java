package com.qsdbl.nazox_demo.entity.qo;

import com.qsdbl.nazox_demo.entity.StoreMat;
import com.qsdbl.nazox_demo.entity.StoreOut;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

/**
 * @description 新增出库单 相关参数
 * @author 轻率的保罗
 * @since 2021-08-26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("新增出库单相关参数")
public class StoreOut_qo implements Serializable {
    @ApiModelProperty("出库单数据")
    private StoreOut storeOut;
    @ApiModelProperty("出库明细数据")
    private StoreMat[] storeMats;
}