package com.qsdbl.nazox_demo.service.store;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qsdbl.nazox_demo.entity.StoreRec;
import com.qsdbl.nazox_demo.entity.qo.PageInfo_qo;
import com.qsdbl.nazox_demo.entity.vo.DataVo;
import com.qsdbl.nazox_demo.mapper.store.StoreIndetMapper;
import com.qsdbl.nazox_demo.mapper.store.StoreRecMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * @description 服务层 - 仓库相关
 * @author 轻率的保罗
 * @since 2021-08-27
 */
@Service
public class StoreService {

    /**
     * 库存查询表 Mapper对象
     */
    @Autowired
    private StoreRecMapper recMapper;

    /**
     * 入库明细表 Mapper对象
     */
    @Autowired
    private StoreIndetMapper indetMapper;


    /**
     * 查询 物资货位
     */
    public DataVo queryAll_LC(String mat_id) {
        DataVo dataVo = new DataVo();
        dataVo.setData(indetMapper.queryByLC(mat_id));
        return dataVo;
    }

    /**
     * 分页查询 库存
     */
    public DataVo queryAll(PageInfo_qo qo) {
        //设置对应的分页参数
        //第一个参数pageNum：需要查询的当前页。pageSize：需要显示的数据量大小
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        //查询
        List<StoreRec> list = recMapper.queryAll();
        //封装成分页数据
        PageInfo pageInfo = new PageInfo(list);
        DataVo dataVo = new DataVo();//视图对象，按规定好的格式返回数据给前端
        dataVo.setData(pageInfo);
        return dataVo;
    }

}
