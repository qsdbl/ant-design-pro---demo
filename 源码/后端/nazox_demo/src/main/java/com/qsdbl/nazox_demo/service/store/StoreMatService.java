package com.qsdbl.nazox_demo.service.store;

import cn.hutool.core.date.DateUtil;
import com.qsdbl.nazox_demo.entity.StoreMat;
import com.qsdbl.nazox_demo.entity.StoreMtype;
import com.qsdbl.nazox_demo.entity.vo.DataVo;
import com.qsdbl.nazox_demo.mapper.store.StoreMatMapper;
import com.qsdbl.nazox_demo.mapper.store.StoreMtypeMapper;
import com.qsdbl.nazox_demo.utils.GenerateID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @description 物资目录表 服务层
 * @author 轻率的保罗
 * @since 2021-08-05
 */
@Service
public class StoreMatService {

    /**
     * 物资目录表 Mapper对象
     */
    @Autowired
    private StoreMatMapper storeMatMapper;

    /**
     * 物资分类表 Mapper对象
     */
    @Autowired
    private StoreMtypeMapper storeMtypeMapper;

    /**
     * @description 新增 物资
     * @param  mat 物资信息
     */
    public DataVo insert(StoreMat mat){
        DataVo dataVo = new DataVo();//视图对象，按规定好的格式返回数据给前端
        String mat_id = GenerateID.storeMatID();//生成物资id
        while (storeMatMapper.queryByID(mat_id)!=null){
            mat_id = GenerateID.storeMatID();
        }
        mat.setMat_id(mat_id);
        if (storeMatMapper.queryByCode(mat.getMat_code())!=null){//判断 物资编码 是否已存在
            dataVo.setSuccess("false");
            dataVo.setMessage("物资编码已存在！");
            return dataVo;
        }
        if(mat.getAdd_date() == null){//添加时间
            mat.setAdd_date(DateUtil.now());
        }
        if (storeMatMapper.insert(mat)>0){
            dataVo.setMessage("物资新增成功！");
        }else{
            dataVo.setSuccess("false");
            dataVo.setMessage("物资新增失败！");
        }
        return dataVo;
    }

    /**
     * @description 删除 物资
     * @param  mat_id 物资id
     */
    public DataVo delById(String mat_id){
        DataVo dataVo = new DataVo();//视图对象，按规定好的格式返回数据给前端
        if (storeMatMapper.delById(mat_id)>0){
            dataVo.setMessage("物资删除成功！");
        }else{
            dataVo.setSuccess("false");
            dataVo.setMessage("物资删除失败！");
        }
        return dataVo;
    }
    /**
     * @description 修改 物资
     * @param  mat 物资信息
     */
    public DataVo update(StoreMat mat){
        DataVo dataVo = new DataVo();//视图对象，按规定好的格式返回数据给前端
//        根据type_id 查找type_code、type_name
        StoreMtype storeMtype = storeMtypeMapper.queryById(mat.getType_id());
        if (storeMtype==null){
            dataVo.setSuccess("false");
            dataVo.setMessage("不存在该分类！");
            return dataVo;
        }else{
            mat.setTypeCode(storeMtype.getType_code());
            mat.setTypeName(storeMtype.getType_name());
        }
        if (storeMatMapper.update(mat)>0){
            dataVo.setMessage("物资修改成功！");
        }else{
            dataVo.setSuccess("false");
            dataVo.setMessage("物资修改失败！");
        }
        return dataVo;
    }

    /**
     * @description 查询数据 通过分类名称
     * @param  type_name 分类名称
     */
    public DataVo queryByTypename(String type_name) {
        DataVo dataVo = new DataVo();//视图对象，按规定好的格式返回数据给前端
        dataVo.setData(storeMatMapper.queryByTypename(type_name));
        return dataVo;
    }

}
