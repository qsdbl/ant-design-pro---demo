package com.qsdbl.nazox_demo.mapper.store;

import com.qsdbl.nazox_demo.entity.StoreMat;
import com.qsdbl.nazox_demo.mapper.provider.StoreMatMapperProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @description 物资目录表 Mapper层
 * @author 轻率的保罗
 * @since 2021-08-03
 */
@Mapper
public interface StoreMatMapper {
    /**
     * @description 新增 物资
     * @param mat 物资信息
     * @return
     */
    @InsertProvider(value = StoreMatMapperProvider.class,method = "insertSql")
    public int insert(StoreMat mat);

    //删除 物资
    @Delete("delete from store_mat where mat_id = #{mat_id}")
    public int delById(@Param("mat_id") String mat_id);

    //修改 物资
    @InsertProvider(value = StoreMatMapperProvider.class,method = "updateSql")
    public int update(StoreMat mat);

    //查询数据 通过分类名称
    @Select("select a.*,b.type_code typeCode,b.type_name typeName from store_mat a,store_mtype b where b.type_name = #{type_name} and b.type_id = a.type_id order by a.add_date desc")
    public List<StoreMat> queryByTypename(@Param("type_name") String type_name);

    //查询数据 通过物资id
    @Select("select * from store_mat where mat_id = #{mat_id}")
    public StoreMat queryByID(@Param("mat_id") String mat_id);

    //查询数据 通过物资编码
    @Select("select * from store_mat where mat_code = #{mat_code}")
    public StoreMat queryByCode(@Param("mat_code") String mat_code);
}
