package com.qsdbl.nazox_demo.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description 物资分类表 实体类
 * @author 轻率的保罗
 * @since 2021-07-26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("物资分类")
public class StoreMtype {
    @ApiModelProperty("主键")
    private String type_id;
    @ApiModelProperty("分类编码")
    private String type_code;
    @ApiModelProperty("分类名称")
    private String type_name;
    @ApiModelProperty("分类级别")
    private String type_level;
    @ApiModelProperty("分类描述")
    private String type_desc="";
    @ApiModelProperty("添加人ID")
    private String add_userid;
    @ApiModelProperty("添加时间")
    private String add_date;
    @ApiModelProperty("修改人ID")
    private String modify_userid;
    @ApiModelProperty("修改时间")
    private String modify_date;
    @ApiModelProperty("系统租户ID")
    private String tenant_id;
    @ApiModelProperty("子节点。用于生成树结构数据")
    private List<StoreMtype> children;//子节点
}
