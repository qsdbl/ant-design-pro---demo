package com.qsdbl.nazox_demo.web.store;

import com.qsdbl.nazox_demo.entity.qo.PageInfo_qo;
import com.qsdbl.nazox_demo.entity.qo.StoreOut_qo;
import com.qsdbl.nazox_demo.entity.vo.DataVo;
import com.qsdbl.nazox_demo.service.store.StoreOutService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @description 出库单表 控制层
 * @author 轻率的保罗
 * @since 2021-08-28
 */
@RestController
@RequestMapping("/nazox_demo/v1/")
@CrossOrigin
@Api(tags = "出库单相关操作")//swagger注解，用于给当前模块添加注释
public class StoreOutController {

    /**
     * 出库单表 Service对象
     */
    @Autowired
    private StoreOutService storeOutService;


    @ApiOperation("添加出库单数据")//swagger注解，用于给当前api（接口）添加注释
    @PostMapping("storeout")
    public DataVo insert(@RequestBody @ApiParam("新增出库单相关参数") StoreOut_qo qo){//@ApiParam - swagger注解，用于给参数添加注释
        return storeOutService.insert(qo.getStoreOut(),qo.getStoreMats());
    }

    /**
     * @description 分页查询 出库单数据
     * @param qo 分页查询相关的数据，包括每页显示的数据大小pageSize（默认为10）、查询的当前页currentPage（默认为1）
     * @return 出库单详细数据（包含物资）
     */
    @ApiOperation("分页查询出库单数据")
    @GetMapping("storeouts")
    public DataVo queryAll(@ApiParam("前端提供的分页参数")PageInfo_qo qo){
        return storeOutService.queryAll(qo);
    }
}