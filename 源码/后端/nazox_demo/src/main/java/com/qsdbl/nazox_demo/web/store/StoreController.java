package com.qsdbl.nazox_demo.web.store;

import com.qsdbl.nazox_demo.entity.qo.PageInfo_qo;
import com.qsdbl.nazox_demo.entity.vo.DataVo;
import com.qsdbl.nazox_demo.service.store.StoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @description 控制层 - 仓库相关
 * @author 轻率的保罗
 * @since 2021-08-27
 */
@RestController
@RequestMapping("/nazox_demo/v1/")
@CrossOrigin
@Api(tags = "仓库相关操作")//swagger注解，用于给当前模块添加注释
public class StoreController {

    /**
     * 库存查询表 Service对象
     */
    @Autowired
    private StoreService recService;

    /**
     * 仓库目录表 Service对象
     */
    @Autowired
    private StoreService storeService;

    @ApiOperation("查询物资货位")
    @GetMapping("store/local_code/{mat_id}")
    public DataVo queryAll_LC(@PathVariable @ApiParam("物资ID") String mat_id){
        return storeService.queryAll_LC(mat_id);
    }

    @ApiOperation("分页查询库存")
    @GetMapping("storerecs")
    public DataVo queryAll_rec(@ApiParam("前端提供的分页参数") PageInfo_qo qo){
        return recService.queryAll(qo);
    }
}