package com.qsdbl.nazox_demo.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description 入库明细表 实体类
 * @author 轻率的保罗
 * @since 2021-07-16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("入库明细表")
public class StoreIndet {
    @ApiModelProperty("物资ID")
    private String mat_id;
    @ApiModelProperty("入库数量")
    private String in_num;
    @ApiModelProperty("入库金额")
    private String in_money;
    @ApiModelProperty("货位")
    private String local_code;
    @ApiModelProperty("入库单ID")
    private String in_id;
    @ApiModelProperty("主键")
    private String in_detid;
    @ApiModelProperty("添加人ID")
    private String add_userid;
    @ApiModelProperty("添加时间")
    private String add_date;
    @ApiModelProperty("修改人ID")
    private String modify_userid;
    @ApiModelProperty("修改时间")
    private String modify_date;
    @ApiModelProperty("系统租户ID")
    private String tenant_id;
}
