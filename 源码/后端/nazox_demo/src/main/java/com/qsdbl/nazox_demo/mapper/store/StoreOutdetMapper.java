package com.qsdbl.nazox_demo.mapper.store;

import com.qsdbl.nazox_demo.entity.StoreIndet;
import com.qsdbl.nazox_demo.entity.StoreOutdet;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @description 出库明细表 Mapper层
 * @author 轻率的保罗
 * @since 2021-08-27
 */
@Mapper
public interface StoreOutdetMapper {

    //添加 出库明细
    @Insert("insert into store_outdet(mat_id,out_num,out_money,out_id,local_code,out_detid) values(#{mat_id},#{out_num},#{out_money},#{out_id},#{local_code},#{out_detid})")
    public int insert(StoreOutdet storeOutdet);

    //通过 出库明细ID 查询
    @Select("select * from store_outdet where out_detid = #{out_detid}")
    public StoreOutdet queryByID(@Param("out_detid") String out_detid);
}
