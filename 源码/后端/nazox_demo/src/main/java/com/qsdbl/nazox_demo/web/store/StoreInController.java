package com.qsdbl.nazox_demo.web.store;

import com.qsdbl.nazox_demo.entity.qo.StoreIn_qo;
import com.qsdbl.nazox_demo.entity.vo.DataVo;
import com.qsdbl.nazox_demo.service.store.StoreInService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.qsdbl.nazox_demo.entity.qo.PageInfo_qo;

/**
 * @description 入库单表 控制层
 * @author 轻率的保罗
 * @since 2021-07-18
 */
@RestController
@RequestMapping("/nazox_demo/v1/")
@CrossOrigin
@Api(tags = "入库单相关操作")//swagger注解，用于给当前模块添加注释
public class StoreInController {

    /**
     * 入库单表 Service对象
     */
    @Autowired
    private StoreInService storeInService;

    @ApiOperation("添加入库单数据")//swagger注解，用于给当前api（接口）添加注释
    @PostMapping("storein")
    public DataVo insert(@RequestBody @ApiParam("新增入库单相关参数") StoreIn_qo qo){//@ApiParam - swagger注解，用于给参数添加注释
        return storeInService.insert(qo.getStoreIn(),qo.getStoreMats());
    }

    @ApiOperation("分页查询入库单数据")
    @GetMapping("storeins")
    public DataVo queryAll(@ApiParam("前端提供的分页参数")PageInfo_qo qo){
        return storeInService.queryAll(qo);
    }
}