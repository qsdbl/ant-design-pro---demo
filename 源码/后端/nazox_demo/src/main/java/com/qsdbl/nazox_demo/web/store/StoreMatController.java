package com.qsdbl.nazox_demo.web.store;

import com.qsdbl.nazox_demo.entity.StoreMat;
import com.qsdbl.nazox_demo.entity.vo.DataVo;
import com.qsdbl.nazox_demo.service.store.StoreMatService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @description 物资目录表 控制层
 * @author 轻率的保罗
 * @since 2021-08-5
 */
@RestController
@RequestMapping("/nazox_demo/v1/")
@CrossOrigin
@Api(tags = "物资目录表相关操作")//swagger注解，用于给当前模块添加注释
public class StoreMatController {

    /**
     * 物资目录表 Service对象
     */
    @Autowired
    private StoreMatService storeMatService;

    /**
     * @description 新增 物资
     * @param mat 物资信息
     * @return
     */
    @ApiOperation("新增物资")//swagger注解，用于给当前api（接口）添加注释
    @PostMapping("storemat/{mat_code}")
    public DataVo insert(@RequestBody @ApiParam("物资数据") StoreMat mat){
        return storeMatService.insert(mat);
    }

    /**
     * @description 删除 物资
     * @param mat_id 物资id
     * @return
     */
    @ApiOperation("删除物资")
    @DeleteMapping("storemat/{mat_id}")
    public DataVo delById(@PathVariable @ApiParam("物资id") String mat_id){
        return storeMatService.delById(mat_id);
    }

    /**
     * @description 修改 物资
     * @param mat 物资信息
     * @return
     */
    @ApiOperation("修改物资")
    @PatchMapping("storemat/{mat_code}")
    public DataVo update(@RequestBody @ApiParam("物资信息") StoreMat mat){
        return storeMatService.update(mat);
    }

    /**
     * @description 查询数据 通过分类名称
     * @param  type_name 分类名称
     */
    @ApiOperation("查询数据，通过分类名称获取")
    @GetMapping("storemats/{type_name}")
    public DataVo queryAll(@PathVariable @ApiParam("分类名称") String type_name){
        return storeMatService.queryByTypename(type_name);
    }

}