package com.qsdbl.nazox_demo.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description 库存查询表 实体类
 * @author 轻率的保罗
 * @since 2021-08-27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("库存查询表")
public class StoreRec {
    @ApiModelProperty("仓库名称")
    private String house_name;
    @ApiModelProperty("仓库编码")
    private String house_code;
    @ApiModelProperty("仓库ID")
    private String house_id;
    @ApiModelProperty("物资ID")
    private String mat_id;
    @ApiModelProperty("物资编码")
    private String mat_code;
    @ApiModelProperty("物资名称")
    private String mat_name;
    @ApiModelProperty("型号")
    private String mat_size;
    @ApiModelProperty("单位")
    private String mat_unit;
    @ApiModelProperty("库存单价")
    private String store_price;
    @ApiModelProperty("库存数量")
    private String store_num;
    @ApiModelProperty("库存金额")
    private String store_money;
    @ApiModelProperty("最近入库单号")
    private String last_code;
    @ApiModelProperty("最近入库日期")
    private String last_date;
    @ApiModelProperty("主键")
    private String store_id;
    @ApiModelProperty("添加人ID")
    private String add_userid;
    @ApiModelProperty("添加时间")
    private String add_date;
    @ApiModelProperty("修改人ID")
    private String modify_ID;
    @ApiModelProperty("修改时间")
    private String modify_date;
    @ApiModelProperty("系统租户ID")
    private String tenant_id;
}
