package com.qsdbl.nazox_demo.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description 仓库目录表 实体类
 * @author 轻率的保罗
 * @since 2021-08-26
 */
@Data//生成get、set等
@AllArgsConstructor//生成有参构造器
@NoArgsConstructor//生成无参构造器
@ApiModel("仓库目录")//swagger注解，用于给类添加注释
public class StoreHouse {
    @ApiModelProperty("仓库编码")//swagger注解，用于给类属性添加注释
    private String house_code;
    @ApiModelProperty("仓库名称")
    private String house_name;
    @ApiModelProperty("仓库描述")
    private String house_desc;
    @ApiModelProperty("主键")
    private String house_id;
    @ApiModelProperty("添加人ID")
    private String add_userid;
    @ApiModelProperty("添加时间")
    private String add_date;
    @ApiModelProperty("修改人ID")
    private String modify_userid;
    @ApiModelProperty("修改时间")
    private String modify_date;
    @ApiModelProperty("系统租户ID")
    private String tenant_id;
}
