package com.qsdbl.nazox_demo.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description 用户表 实体类
 * @author 轻率的保罗
 * @since 2021-08-26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("用户表")
public class SysUser {
    @ApiModelProperty("用户ID")
    private String user_id;
    @ApiModelProperty("用户姓名")
    private String user_name;
    @ApiModelProperty("账号")
    private String user_code;
    @ApiModelProperty("密码")
    private String user_pwd;
}
