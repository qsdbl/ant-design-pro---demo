package com.qsdbl.nazox_demo.mapper.sys;

import com.qsdbl.nazox_demo.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @description 用户表 Mapper层
 * @author 轻率的保罗
 * @since 2021-08-26
 */
@Mapper
public interface SysUserMapper {
    //查询所有数据
    @Select("select * from sys_user")
    public List<SysUser> queryAll();
}
