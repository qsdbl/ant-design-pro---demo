package com.qsdbl.nazox_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NazoxDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(NazoxDemoApplication.class, args);
    }

}
