package com.qsdbl.nazox_demo.entity.qo;

import com.qsdbl.nazox_demo.entity.StoreIn;
import com.qsdbl.nazox_demo.entity.StoreMat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

/**
 * @description 新增入库单 相关参数
 * @author 轻率的保罗
 * @since 2021-08-26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("新增入库单相关参数")
public class StoreIn_qo implements Serializable {
    @ApiModelProperty("入库单数据")
    private StoreIn storeIn;
    @ApiModelProperty("入库明细数据")
    private StoreMat[] storeMats;
}