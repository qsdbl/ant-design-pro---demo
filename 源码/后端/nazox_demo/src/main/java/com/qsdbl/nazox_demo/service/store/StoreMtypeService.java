package com.qsdbl.nazox_demo.service.store;

import com.qsdbl.nazox_demo.entity.StoreMat;
import com.qsdbl.nazox_demo.entity.StoreMtype;
import com.qsdbl.nazox_demo.entity.vo.DataVo;
import com.qsdbl.nazox_demo.mapper.store.StoreMatMapper;
import com.qsdbl.nazox_demo.mapper.store.StoreMtypeMapper;
import com.qsdbl.nazox_demo.utils.GenerateID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Iterator;
import java.util.List;

/**
 * @description 物资分类表 服务层
 * @author 轻率的保罗
 * @since 2021-07-26
 */
@Service
public class StoreMtypeService {

    /**
     * 物资分类表 Mapper对象
     */
    @Autowired
    private StoreMtypeMapper storeMtypeMapper;

    /**
     * 物资目录表 Mapper对象
     */
    @Autowired
    private StoreMatMapper matMapper;

    /**
     * 新增
     */
    public DataVo insert(StoreMtype mtype){
        DataVo dataVo = new DataVo();//视图对象，按规定好的格式返回数据给前端
        if (mtype.getType_code() == null || mtype.getType_name() == null){
            dataVo.setSuccess("false");
            dataVo.setMessage("必须提供分类编码与分类名称！");
            return dataVo;
        }
        if (storeMtypeMapper.queryByCode(mtype.getType_code()) != null){
            dataVo.setSuccess("false");
            dataVo.setMessage("分类编码已存在！");
            return dataVo;
        }
        if (storeMtypeMapper.queryByName(mtype.getType_name()) != null){
            dataVo.setSuccess("false");
            dataVo.setMessage("存在同名的分类！");
            return dataVo;
        }
        //生成 ID
        String type_id = GenerateID.randomChar(7);
        while (storeMtypeMapper.queryById(type_id)!=null){
            type_id = GenerateID.randomChar(7);
        }
        mtype.setType_id(type_id);
        if (storeMtypeMapper.insert(mtype)>0){
            dataVo.setMessage("创建成功！");
        }else{
            dataVo.setSuccess("false");
            dataVo.setMessage("创建失败！");
        }
        return dataVo;
    }

    /**
     * 删除某条分类数据，及其子项目
     */
    public DataVo delete(String type_code){
        DataVo dataVo = new DataVo();//视图对象，按规定好的格式返回数据给前端
        if (type_code != null || "".equals(type_code)){
            StoreMtype storeMtype = storeMtypeMapper.queryByCode(type_code);
            List<StoreMat> storeMats = matMapper.queryByTypename(storeMtype.getType_name());
            if(storeMats.size()>0){
                dataVo.setSuccess("false");
                dataVo.setMessage("当前分类下存在物资，不能删除！");
                return dataVo;
            }
            if (storeMtypeMapper.delete(type_code)>0){
                dataVo.setMessage("删除成功！");
            }else{
                dataVo.setSuccess("false");
                dataVo.setMessage("删除失败！");
            }
            return dataVo;
        }
        dataVo.setSuccess("false");
        dataVo.setMessage("必须提供分类编码！");
        return dataVo;
    }

    /**
     * 修改
     */
    public DataVo update(StoreMtype mtype){
        DataVo dataVo = new DataVo();//视图对象，按规定好的格式返回数据给前端
        if (mtype.getType_id() == null){
            dataVo.setSuccess("false");
            dataVo.setMessage("必须提供id！");
            return dataVo;
        }
        StoreMtype mtype_db = storeMtypeMapper.queryById(mtype.getType_id());
        if (mtype_db == null){
            dataVo.setSuccess("false");
            dataVo.setMessage("id不存在！");
            return dataVo;
        }
        if(mtype.getType_code()!=null){
            StoreMtype mtype_db2 = storeMtypeMapper.queryByCode(mtype.getType_code());
            if (mtype_db2 != null && !mtype_db2.getType_id().equals(mtype.getType_id())){
                dataVo.setSuccess("false");
                dataVo.setMessage("分类编码已存在！");
                return dataVo;
            }
            String code = mtype.getType_code();
            if (code.length()>2){
                if (mtype.getType_code().length()%2==0){
                    code = mtype.getType_code().substring(0,mtype.getType_code().length()-2);
                }else{
                    code = mtype.getType_code().substring(0,mtype.getType_code().length()-1);
                }
                if (storeMtypeMapper.queryByCode(code) == null){
                    dataVo.setSuccess("false");
                    dataVo.setMessage("上一节点\""+code+"\"不存在！");
                    return dataVo;
                }
            }
            mtype_db.setType_code(mtype.getType_code());
        }
        if(mtype.getType_name()!=null){
            mtype_db.setType_name(mtype.getType_name());
        }
        if(mtype.getType_desc()!=null){
            mtype_db.setType_desc(mtype.getType_desc());
        }
        if (storeMtypeMapper.update(mtype_db)>0){
            dataVo.setMessage("修改成功！");
        }else{
            dataVo.setSuccess("false");
            dataVo.setMessage("修改失败！");
        }
        return dataVo;
    }

    /**
     * 查询 物资分类（数据为树结构）
     */
    public DataVo queryAll(){
        DataVo dataVo = new DataVo();//视图对象，按规定好的格式返回数据给前端
        StoreMtype mtype = new StoreMtype();
        mtype.setChildren(storeMtypeMapper.queryLevel(2));//2位一级，查询出一级菜单
        List<StoreMtype> mtypeTree = createMtypeTree(mtype).getChildren();//全部菜单（树）
        dataVo.setData(mtypeTree);
        return dataVo;
    }

    /**
     * 查找其子菜单（递归函数）
     */
    public StoreMtype createMtypeTree(StoreMtype mtype){
        if (mtype.getChildren() != null){
            //如果存在子菜单 则遍历子菜单 并为子菜单查找其子菜单
            Iterator<StoreMtype> iterator = mtype.getChildren().iterator();
            while (iterator.hasNext()){
                StoreMtype mtype_children = iterator.next();
                //查询当前 分类编码 下的所有子菜单
                mtype_children.setChildren(storeMtypeMapper.queryLevelAll(mtype_children.getType_code()));
                createMtypeTree(mtype_children);//递归函数，还会继续为子菜单查找其子菜单
            }
        }
        return mtype;
    }
}
