package com.qsdbl.nazox_demo.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description 物资表 实体类
 * @author 轻率的保罗
 * @since 2021-07-16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("物资目录")
public class StoreMat {
    @ApiModelProperty("物资编码")
    private String mat_code;
    @ApiModelProperty("物资名称")
    private String mat_name;
    @ApiModelProperty("物资型号")
    private String mat_size;
    @ApiModelProperty("物资单位")
    private String mat_unit;
    @ApiModelProperty("物资单价")
    private String mat_price;
    @ApiModelProperty("备注")
    private String mat_desc;
    @ApiModelProperty("主键")
    private String mat_id;
    @ApiModelProperty("分类ID")
    private String type_id;
    @ApiModelProperty("分类编码")
    private String typeCode;//从物资分类表获取
    @ApiModelProperty("分类名称")
    private String typeName;//从物资分类表获取
    @ApiModelProperty("添加人ID")
    private String add_userid;
    @ApiModelProperty("添加时间")
    private String add_date;
    @ApiModelProperty("修改人ID")
    private String modify_userid;
    @ApiModelProperty("修改时间")
    private String modify_date;
    @ApiModelProperty("系统租户ID")
    private String tenant_id;
    @ApiModelProperty("入库数量。入库明细中使用到，来自入库明细表")
    private String in_num;
    @ApiModelProperty("入库金额。入库明细中使用到")
    private String in_money;
    @ApiModelProperty("出库数量。出库明细中使用到，来自出库明细表")
    private String out_num;
    @ApiModelProperty("出库金额。出库明细中使用到")
    private String out_money;
    @ApiModelProperty("货位。入库明细中使用到")
    private String local_code;
}
