package com.qsdbl.nazox_demo.web.store;

import com.qsdbl.nazox_demo.entity.StoreMtype;
import com.qsdbl.nazox_demo.entity.vo.DataVo;
import com.qsdbl.nazox_demo.service.store.StoreMtypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @description 物资分类表 控制层
 * @author 轻率的保罗
 * @since 2021-07-26
 */
@RestController
@RequestMapping("/nazox_demo/v1/")
@CrossOrigin
@Api(tags = "物资分类表相关操作")//swagger注解，用于给当前模块添加注释
public class StoreMtypeController {

    /**
     * 物资分类表 Service对象
     */
    @Autowired
    private StoreMtypeService storeMtypeService;

    /**
     * 新增
     * 必须提供 分类编码type_code、分类名称type_name
     */
    @ApiOperation("新增物资分类")//swagger注解，用于给当前api（接口）添加注释
    @PostMapping("storemtype/{type_code}")
    public DataVo insert(@RequestBody @ApiParam("物资分类数据，必须提供分类编码type_code、分类名称type_name") StoreMtype mtype){
        return storeMtypeService.insert(mtype);
    }

    /**
     * 删除某条数据
     */
    @ApiOperation("删除数据")
    @DeleteMapping("storemtype/{type_code}")
    public DataVo delete(@PathVariable @ApiParam("分类编码") String type_code){
        return storeMtypeService.delete(type_code);
    }

    /**
     * 修改
     * 必须提供 分类编码type_code、分类名称type_name、分类描述type_desc、id
     */
    @ApiOperation("修改数据")
    @PatchMapping("storemtype/{type_code}")
    public DataVo update(@RequestBody  @ApiParam("物资分类数据，必须提供分类编码type_code、分类名称type_name、分类描述type_desc、id") StoreMtype mtype){
        return storeMtypeService.update(mtype);
    }

    /**
     * 查询 物资分类（数据为树结构）
     */
    @ApiOperation("查询物资分类（数据为树结构）")
    @GetMapping("storemtypes")
    public DataVo queryAll(){
        return storeMtypeService.queryAll();
    }
}