package com.qsdbl.nazox_demo.entity.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

/**
 * @description 接收分页查询相关的参数
 * @author 轻率的保罗
 * @since 2021-07-16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("接收分页查询相关的参数")
public class PageInfo_qo implements Serializable{
    @ApiModelProperty("每页显示的数据大小。默认值为10")
    private Integer pageSize = 10;
    @ApiModelProperty("查询的当前页。默认值为1")
    private Integer currentPage = 1;
}