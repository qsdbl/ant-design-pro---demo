package com.qsdbl.nazox_demo.mapper.store;

import com.qsdbl.nazox_demo.entity.StoreOut;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import java.util.List;

/**
 * @description 出库单表 Mapper层
 * @author 轻率的保罗
 * @since 2021-08-27
 */
@Mapper
public interface StoreOutMapper {

    //添加 出库单
    @Insert("insert into store_out(out_code,out_id,house_name,edit_user,send_user,out_desc,out_money,add_date,modify_date,out_date,modify_userid,add_userid,house_id,edit_userid,pur_user,pur_userid,auditing) values(#{out_code},#{out_id},#{house_name},#{edit_user},#{send_user},#{out_desc},#{out_money},#{add_date},#{modify_date},#{out_date},#{modify_userid},#{add_userid},#{house_id},#{edit_userid},#{pur_user},#{pur_userid},#{auditing})")
    public int insert(StoreOut storeOut);

    //查询所有数据
    public List<StoreOut> queryAll();

    //通过 单号 查询
    @Select("select * from store_out where out_code = #{out_code}")
    public StoreOut queryByCode(@Param("out_code") String out_code);

    //通过 出库单ID 查询
    @Select("select * from store_out where out_id = #{out_id}")
    public StoreOut queryByID(@Param("out_id") String out_id);
}
