package com.qsdbl.nazox_demo.mapper.store;

import com.qsdbl.nazox_demo.entity.StoreMtype;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @description 物资分类表 Mapper层
 * @author 轻率的保罗
 * @since 2021-07-26
 */
@Mapper
public interface StoreMtypeMapper {

    //新增
    @Insert("insert into store_mtype(type_code,type_name,type_desc,type_id) values(#{type_code},#{type_name},#{type_desc},#{type_id})")
    public int insert(StoreMtype mtype);

    //删除某条数据
    @Delete("delete from store_mtype where type_code like #{type_code}\"%\"")
    public int delete(@Param("type_code") String type_code);

    //修改
    @Insert("update store_mtype set type_code = #{type_code},type_name = #{type_name},type_desc = #{type_desc} where type_id = #{type_id}")
    public int update(StoreMtype mtype);

    //查询 是否已存在 通过编码
    @Select("select * from store_mtype where type_code = #{type_code}")
    public StoreMtype queryByCode(@Param("type_code") String type_code);

    //查询 是否已存在 通过id
    @Select("select * from store_mtype where type_id = #{type_id}")
    public StoreMtype queryById(@Param("type_id") String type_id);

    //查询 是否存在同名
    @Select("select * from store_mtype where type_name = #{type_name}")
    public StoreMtype queryByName(@Param("type_name") String type_name);

    //查询 某一级 的数据（两位一级，一级目录num=2，二级目录num=4，...）
    @Select("select * from store_mtype where type_code is not null and length(type_code) = #{num}")
    public List<StoreMtype> queryLevel(@Param("num") int num);

    //查询 某一级下的 数据（根据分类编码进行查询）
    @Select("select * from store_mtype where type_code is not null and length(type_code) > length(#{type_code}) and length(type_code) <= length(#{type_code})+2 and type_code like #{type_code}\"%\"")
    public List<StoreMtype> queryLevelAll(@Param("type_code") String type_code);

}
