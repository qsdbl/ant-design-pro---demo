package com.qsdbl.nazox_demo.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @description 配置 redis
 * @author 轻率的保罗
 * @since 2022/4/12
 */
@Configuration
public class RedisConfig {
    //编写配置类 redisTemplate，参考RedisAutoConfiguration的内部类RedisTemplate
    //方法名为 redisTemplate 则bean名也是这个
    @Bean
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<Object, Object> template = new RedisTemplate();




        template.setConnectionFactory(redisConnectionFactory);
        return template;
    }
}
