package com.qsdbl.nazox_demo.utils;

import java.util.Calendar;
import java.util.UUID;

/**
 * @description 生成 单号,id等
 * @author 轻率的保罗
 * @since 2021-07-18
 */
public class GenerateID {
    public static String storeMatID(){//生成物资id
        return "jxstar-"+String.valueOf(UUID.randomUUID()).substring(0,3)+"-"+ String.valueOf(UUID.randomUUID()).substring(0,3);
    }
    public static String storeInCode(String qz){//生成入库单 单号。参数为单号前缀
        StringBuffer in_code = new StringBuffer();
        in_code.append(qz);
        Calendar cal = Calendar.getInstance();
        in_code.append(cal.get(Calendar.YEAR));
        int month = cal.get(Calendar.MONTH) + 1;
        in_code.append(month<10?"0"+month:month);
        in_code.append((int)((Math.random()*9+1)*100000));
        return in_code.toString();
    }
    public static String randomChar(int num){//随机字符，num-位数
        return UUID.randomUUID().toString().substring(0,num);
    }
}
