package com.qsdbl.nazox_demo.mapper.store;

import com.qsdbl.nazox_demo.entity.StoreIn;
import org.apache.ibatis.annotations.*;
import java.util.List;

/**
 * @description 入库单表 Mapper层
 * @author 轻率的保罗
 * @since 2021-07-16
 */
@Mapper
public interface StoreInMapper {

    //添加 入库单
    @Insert("insert into store_in(in_code,in_id,house_name,edit_user,send_user,in_desc,in_money,add_date,modify_date,in_date,modify_userid,pur_userid,add_userid,house_id,edit_userid,pur_user,auditing) values(#{in_code},#{in_id},#{house_name},#{edit_user},#{send_user},#{in_desc},#{in_money},#{add_date},#{modify_date},#{in_date},#{modify_userid},#{pur_userid},#{add_userid},#{house_id},#{edit_userid},#{pur_user},#{auditing})")
    public int insert(StoreIn storeIn);

    //删除 (测试)
    @Delete("delete from store_in where in_id = #{in_id}")
    public int delete(@Param("in_id") String in_id);

    //查询所有数据
    public List<StoreIn> queryAll();

    //通过 单号 查询
    @Select("select * from store_in where in_code = #{in_code}")
    public StoreIn queryByCode(@Param("in_code") String in_code);

    //通过 入库单ID 查询
    @Select("select * from store_in where in_id = #{in_id}")
    public StoreIn queryByID(@Param("in_id") String in_id);
}
