package com.qsdbl.nazox_demo.service.store;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qsdbl.nazox_demo.entity.*;
import com.qsdbl.nazox_demo.entity.qo.PageInfo_qo;
import com.qsdbl.nazox_demo.entity.vo.DataVo;
import com.qsdbl.nazox_demo.mapper.store.*;
import com.qsdbl.nazox_demo.utils.GenerateID;
import com.qsdbl.nazox_demo.utils.MyMath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @description 出库单表 服务层
 * @author 轻率的保罗
 * @since 2021-08-28
 */
@Service
public class StoreOutService {

    /**
     * 出库单表 Mapper对象
     */
    @Autowired
    private StoreOutMapper storeOutMapper;

    /**
     * 出库明细表 Mapper对象
     */
    @Autowired
    private StoreOutdetMapper outdetMapper;

    /**
     * 库存查询表 Mapper对象
     */
    @Autowired
    private StoreRecMapper recMapper;

    /**
     * @description 新增 出库单（需要更新 出库单、出库明细表、库存查询表）
     * @param  storeOut 出库单信息
     * @param  storeMats 出库明细数据
     */
    @Transactional//开启事务
    public DataVo insert(StoreOut storeOut,StoreMat[] storeMats){
        DataVo dataVo = new DataVo();//视图对象，按规定好的格式返回数据给前端
        //检查库存
        String mat_lack = "";
        for (int i = 0; i < storeMats.length; i++) {
            StoreRec rec = recMapper.queryByHM(storeOut.getHouse_id(), storeMats[i].getMat_id());
            if (rec == null || Integer.valueOf(MyMath.subZeroAndDot(rec.getStore_num()))<Integer.valueOf(MyMath.subZeroAndDot(storeMats[i].getOut_num()))){
                //库存不足
                mat_lack += storeMats[i].getMat_name()+"、";
            }
        }
        if (mat_lack.length()>0){//库存不足
            dataVo.setMessage("仓库\""+storeOut.getHouse_name()+"\"中，以下物资库存不足：[ "+mat_lack.substring(0,mat_lack.length()-1)+" ]");
            dataVo.setSuccess("false");
            return dataVo;
        }
        //生成 单号
        String out_code = GenerateID.storeInCode("CK");
        while (storeOutMapper.queryByCode(out_code)!=null){
            out_code = GenerateID.storeInCode("CK");
        }
        storeOut.setOut_code(out_code);
        //生成 出库ID
        String out_id = GenerateID.storeMatID();
        while (storeOutMapper.queryByID(out_id)!=null){
            out_id = GenerateID.storeMatID();
        }
        String now_date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        storeOut.setModify_date(now_date);
        storeOut.setAdd_date(now_date);
        storeOut.setOut_date(now_date);
        storeOut.setOut_id(out_id);
        storeOut.setModify_userid(storeOut.getPur_userid());//新增数据时 制单人就是修改人
        storeOut.setAdd_userid(storeOut.getPur_userid());//新增数据时 制单人就是添加人
        if(storeOutMapper.insert(storeOut)<=0){//新增出库单
            dataVo.setSuccess("false");
            dataVo.setMessage("出库单,添加失败！");
            return dataVo;
        }
        return updateIM(storeOut,storeMats,out_id,out_code,now_date,dataVo);//更新 出库明细、库存查询
    }

    /**
     * @description 更新 出库明细、库存查询
     * @param storeMats 出库单所包含的物资
     * @param out_id 出库单 主键
     * @param out_code 出库单 单号
     */
    @Transactional//开启事务
    public DataVo updateIM(StoreOut storeOut,StoreMat[] storeMats,String out_id,String out_code,String now_date,DataVo dataVo){
        for (int i = 0; i < storeMats.length; i++) {
            //新增出库明细：
            StoreOutdet outdet = new StoreOutdet();
            //生成 出库明细ID
            String out_detid = GenerateID.storeMatID();
            while (outdetMapper.queryByID(out_detid)!=null){
                out_detid = GenerateID.storeMatID();
            }
            outdet.setOut_detid(out_detid);
            outdet.setMat_id(storeMats[i].getMat_id());
            outdet.setOut_num(storeMats[i].getOut_num());
            outdet.setOut_money(storeMats[i].getOut_money());
            outdet.setLocal_code(storeMats[i].getLocal_code());
            outdet.setOut_id(out_id);
            if(outdetMapper.insert(outdet)<=0){
                dataVo.setSuccess("false");
                dataVo.setMessage("出库明细,添加失败！");
                return dataVo;
            }
            //新增 库存查询：
            StoreRec rec_db = recMapper.queryByHM(storeOut.getHouse_id(), storeMats[i].getMat_id());//查看是否已有记录
            StoreRec rec = new StoreRec();
            if (rec_db != null){
                rec = rec_db;
            }
            //生成 库存查询ID
            String store_id = GenerateID.storeMatID();
            while (recMapper.queryByID(store_id)!=null){
                store_id = GenerateID.storeMatID();
            }
            rec.setStore_id(store_id);
            rec.setLast_code(out_code);
            rec.setLast_date(now_date);
            rec.setAdd_date(now_date);
            rec.setModify_date(now_date);
            rec.setMat_unit(storeMats[i].getMat_unit());
            rec.setMat_size(storeMats[i].getMat_size());
            rec.setMat_name(storeMats[i].getMat_name());
            rec.setMat_code(storeMats[i].getMat_code());
            rec.setMat_id(storeMats[i].getMat_id());
            rec.setHouse_id(storeOut.getHouse_id());
            rec.setHouse_code(storeOut.getHouse_code());
            rec.setHouse_name(storeOut.getHouse_name());
            if(rec_db == null){
                //新增数据
                rec.setStore_num(storeMats[i].getOut_num());//数量
                rec.setStore_price(storeMats[i].getMat_price());//物资单价
                rec.setStore_money(MyMath.subZeroAndDot(Double.valueOf(storeMats[i].getOut_num())*Double.valueOf(storeMats[i].getMat_price())));
                if(recMapper.insert(rec)<=0){
                    dataVo.setSuccess("false");
                    dataVo.setMessage("库存查询,添加失败！");
                    return dataVo;
                }
            }else{
                //更新数据
                rec.setStore_num(MyMath.subZeroAndDot(Double.valueOf(rec_db.getStore_num())-Double.valueOf(storeMats[i].getOut_num())));
                rec.setStore_price(storeMats[i].getMat_price());//物资单价
                rec.setStore_money(MyMath.subZeroAndDot(Double.valueOf(rec_db.getStore_num())*Double.valueOf(rec_db.getStore_price())));
                if(recMapper.update(rec)<=0){
                    dataVo.setSuccess("false");
                    dataVo.setMessage("库存查询,更新数据失败！");
                    return dataVo;
                }
            }
        }
        dataVo.setMessage("添加成功！");
        return dataVo;
    }

    /**
     * @description 分页查询 出库单数据
     * @param qo 分页查询相关的数据，包括每页显示的数据大小pageSize（默认为10）、查询的当前页currentPage（默认为1）
     * @return 出库单详细数据（包含物资）
     */
    public DataVo queryAll(PageInfo_qo qo) {
        //设置对应的分页参数
        //第一个参数pageNum：需要查询的当前页。pageSize：需要显示的数据量大小
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        //查询
        List<StoreOut> list = storeOutMapper.queryAll();
        //封装成分页数据
        PageInfo pageInfo = new PageInfo(list);
        DataVo dataVo = new DataVo();//视图对象，按规定好的格式返回数据给前端
        dataVo.setData(pageInfo);
        return dataVo;
    }

}
