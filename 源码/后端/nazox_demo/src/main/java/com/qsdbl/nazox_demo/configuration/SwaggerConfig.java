package com.qsdbl.nazox_demo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * @description 配置 Swagger2
 * @author 轻率的保罗
 * @since 2021-08-22
 */
@Configuration //配置类
@EnableSwagger2// 开启Swagger2的自动配置
public class SwaggerConfig {
    @Bean //配置docket以配置Swagger具体参数
    public Docket docket(Environment environment) {
        // 设置要启用swagger的环境
        Profiles profiles = Profiles.of("dev", "test");//开发环境、测试环境
        // 判断当前是否处于该环境
        // 通过 enable() 接收此参数判断是否要启用swagger
        boolean b = environment.acceptsProfiles(profiles);

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(myApiInfo())//提供apiInfo，具体配置见下边的myApiInfo方法
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.qsdbl.nazox_demo.web"))//只扫描controller
                .paths(PathSelectors.ant("/nazox_demo/v1/**"))//只扫描该路径下的接口
                .build()
                .enable(b)//配置是否启用Swagger，如果是false，在浏览器将无法访问
                .groupName("全部API");
    }
    @Bean //分组2，仓库API
    public Docket docket1(Environment environment) {
        // 设置要启用swagger的环境
        Profiles profiles = Profiles.of("dev", "test");//开发环境、测试环境
        // 判断当前是否处于该环境
        // 通过 enable() 接收此参数判断是否要启用swagger
        boolean b = environment.acceptsProfiles(profiles);
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(myApiInfo())//提供apiInfo，具体配置见下边的myApiInfo方法
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.qsdbl.nazox_demo.web"))//只扫描controller
                .paths(PathSelectors.regex("^/nazox_demo/v1/store.*"))//只扫描该路径下的接口
                .build()
                .enable(b)//配置是否启用Swagger，如果是false，在浏览器将无法访问
                .groupName("仓库API");
    }
    @Bean //分组3，用户服务API
    public Docket docket2(Environment environment) {
        // 设置要启用swagger的环境
        Profiles profiles = Profiles.of("dev", "test");//开发环境、测试环境
        // 判断当前是否处于该环境
        // 通过 enable() 接收此参数判断是否要启用swagger
        boolean b = environment.acceptsProfiles(profiles);
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(myApiInfo())//提供apiInfo，具体配置见下边的myApiInfo方法
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.qsdbl.nazox_demo.web"))//只扫描controller
                .paths(PathSelectors.regex("^/nazox_demo/v1/userservice.*"))//只扫描该路径下的接口
                .build()
                .enable(b)//配置是否启用Swagger，如果是false，在浏览器将无法访问
                .groupName("用户服务API");
    }

    //配置文档信息
    private ApiInfo myApiInfo() {
        //作者信息
        Contact contact = new Contact("轻率的保罗", "https://qsdbl.gitee.io/", "1135637451@qq.com");
        return new ApiInfo(
                "nazox_demo API文档", // 标题
                "nazox_demo项目的API文档！", // 描述
                "v1.0", // 版本
                "https://qsdbl.gitee.io/", // 组织链接
                contact, // 联系人信息
                "Apach 2.0", // 许可
                "http://www.apache.org/licenses/LICENSE-2.0", // 许可连接
                new ArrayList<>()// 扩展
        );
    }
}
