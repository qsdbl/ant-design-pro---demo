package com.qsdbl.nazox_demo.service.store;

import cn.hutool.core.date.DateUtil;
import com.qsdbl.nazox_demo.entity.StoreHouse;
import com.qsdbl.nazox_demo.entity.vo.DataVo;
import com.qsdbl.nazox_demo.mapper.store.StoreHouseMapper;
import com.qsdbl.nazox_demo.utils.GenerateID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @description 仓库目录表 服务层
 * @author 轻率的保罗
 * @since 2021-08-26
 */
@Service
public class StoreHouseService {

    /**
     * 仓库目录表 Mapper对象
     */
    @Autowired
    private StoreHouseMapper storeHouseMapper;

    /**
     * @description 新增
     * @param house 仓库信息
     */
    public DataVo insert(StoreHouse house){
        DataVo dataVo = new DataVo();//视图对象，按规定好的格式返回数据给前端
        if (house.getHouse_code() == null || house.getHouse_name() == null){
            dataVo.setSuccess("false");
            dataVo.setMessage("必须提供编码与名称！");
            return dataVo;
        }
        if (storeHouseMapper.queryByCode(house.getHouse_code()) != null){
            dataVo.setSuccess("false");
            dataVo.setMessage("编码已存在！");
            return dataVo;
        }
        if (storeHouseMapper.queryByName(house.getHouse_name()) != null){
            dataVo.setSuccess("false");
            dataVo.setMessage("存在同名的仓库！");
            return dataVo;
        }
        if(house.getAdd_date() == null){//添加时间
            house.setAdd_date(DateUtil.now());
        }
        //生成 ID
        String house_id = GenerateID.storeMatID();
        while (storeHouseMapper.queryById(house_id)!=null){
            house_id = GenerateID.storeMatID();
        }
        house.setHouse_id(house_id);
        if(storeHouseMapper.insert(house)>0){
            dataVo.setMessage("创建成功！");
        }else{
            dataVo.setSuccess("false");
            dataVo.setMessage("创建失败！");
        }
        return dataVo;
    }

    /**
     * @description 删除
     * @param house_id 仓库id
     */
    public DataVo delete(String house_id){
        DataVo dataVo = new DataVo();//视图对象，按规定好的格式返回数据给前端
        if (house_id != null || "".equals(house_id)){
            if (storeHouseMapper.delete(house_id)>0){
                dataVo.setMessage("删除成功！");
            }else{
                dataVo.setSuccess("false");
                dataVo.setMessage("删除失败！");
            }
            return dataVo;
        }
        dataVo.setSuccess("false");
        dataVo.setMessage("必须提供id！");
        return dataVo;
    }

    /**
     * 修改
     */
    public DataVo update(StoreHouse house){
        DataVo dataVo = new DataVo();//视图对象，按规定好的格式返回数据给前端
        if (house.getHouse_id() == null){
            dataVo.setSuccess("false");
            dataVo.setMessage("必须提供id！");
            return dataVo;
        }
        StoreHouse house_db = storeHouseMapper.queryById(house.getHouse_id());
        if (house_db == null){
            dataVo.setSuccess("false");
            dataVo.setMessage("id不存在！");//不存在该仓库，无法修改
            return dataVo;
        }
        if(house.getHouse_code()!=null){
            StoreHouse house_db2 = storeHouseMapper.queryByCode(house.getHouse_code());
            if (house_db2 != null && !house_db2.getHouse_id().equals(house.getHouse_id())){
                dataVo.setSuccess("false");
                dataVo.setMessage("编码已存在！");
                return dataVo;
            }
            house_db.setHouse_code(house.getHouse_code());
        }
        if(house.getHouse_name()!=null){
            house_db.setHouse_name(house.getHouse_name());
        }
        if(house.getHouse_desc()!=null){
            house_db.setHouse_desc(house.getHouse_desc());
        }
        if (storeHouseMapper.update(house_db)>0){
            dataVo.setMessage("修改成功！");
        }else{
            dataVo.setSuccess("false");
            dataVo.setMessage("修改失败！");
        }
        return dataVo;
    }

    /**
     * 查询 仓库目录
     */
    public DataVo queryAll_house() {
        DataVo dataVo = new DataVo();
        dataVo.setData(storeHouseMapper.queryAll());
        return dataVo;
    }
}
