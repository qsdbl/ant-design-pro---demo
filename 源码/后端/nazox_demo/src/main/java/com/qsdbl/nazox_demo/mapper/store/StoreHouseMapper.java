package com.qsdbl.nazox_demo.mapper.store;

import com.qsdbl.nazox_demo.entity.StoreHouse;
import org.apache.ibatis.annotations.*;
import java.util.List;

/**
 * @description 仓库目录表 Mapper层
 * @author 轻率的保罗
 * @since 2021-08-27
 */
@Mapper
public interface StoreHouseMapper {
    //新增
    @Insert("insert into store_house(house_code,house_name,house_desc,house_id,add_date) values(#{house_code},#{house_name},#{house_desc},#{house_id},#{add_date})")
    public int insert(StoreHouse house);

    //删除
    @Delete("delete from store_house where house_id = #{house_id}")
    public int delete(@Param("house_id") String house_id);

    //修改
    @Insert("update store_house set house_code = #{house_code},house_name = #{house_name},house_desc = #{house_desc} where house_id = #{house_id}")
    public int update(StoreHouse house);

    //查询所有数据
    @Select("select * from store_house order by house_code")
    public List<StoreHouse> queryAll();

    //查询 是否已存在 通过编码
    @Select("select * from store_house where house_code = #{house_code}")
    public StoreHouse queryByCode(@Param("house_code") String house_code);

    //查询 是否已存在 通过id
    @Select("select * from store_house where house_id = #{house_id}")
    public StoreHouse queryById(@Param("house_id") String house_id);

    //查询 是否存在同名
    @Select("select * from store_house where house_name = #{house_name}")
    public StoreHouse queryByName(@Param("house_name") String house_name);
}
