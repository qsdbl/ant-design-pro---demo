package com.qsdbl.nazox_demo.mapper.provider;

import com.qsdbl.nazox_demo.entity.StoreMat;

/**
 * @description 拼接SQL语句(测试)
 * @author 轻率的保罗
 * @since 2021-08-20
 */
public class StoreMatMapperProvider {

    public String insertSql(StoreMat mat){//新增sql
        StringBuilder sql = new StringBuilder();//sql语句
        StringBuilder fields = new StringBuilder();//字段
        StringBuilder values = new StringBuilder();//值
        sql.append("insert into store_mat");
        fields.append("(mat_id,mat_code,mat_size,mat_name,mat_price,mat_unit,type_id,type_name,type_code,add_date");
        values.append("(\""+mat.getMat_id()+"\",\""+mat.getMat_code()+"\",\""+mat.getMat_size()+"\",\""+mat.getMat_name()+"\",\""+mat.getMat_price()+"\",\""+mat.getMat_unit()+"\",\""+mat.getType_id()+"\",\""+mat.getTypeName()+"\",\""+mat.getTypeCode()+"\",\""+mat.getAdd_date()+"\"");
        //物资描述（非必须）
        if (mat.getMat_desc() != null && mat.getMat_desc().length() > 0){
            fields.append(",mat_desc");
            values.append(",\""+mat.getMat_desc()+"\"");
        }
        return sql.toString()+fields.toString()+")"+" values"+values.toString()+")";
    }
    public String updateSql(StoreMat mat){//修改
        StringBuilder sql = new StringBuilder();//sql语句
        sql.append("update store_mat set ");
        //物资名称
        if (mat.getMat_name() != null){
            sql.append("mat_name=\""+mat.getMat_name()+"\",");
        }
        //物资型号
        if (mat.getMat_size() != null){
            sql.append("mat_size=\""+mat.getMat_size()+"\",");
        }
        //分类
        if (mat.getType_id() != null){
            sql.append("type_id=\""+mat.getType_id()+"\",");
            sql.append("type_name=\""+mat.getTypeName()+"\",");
            sql.append("type_code=\""+mat.getTypeCode()+"\",");
        }
        //物资单价
        if (mat.getMat_price() != null){
            sql.append("mat_price=\""+mat.getMat_price()+"\",");
        }
        //物资单位
        if (mat.getMat_unit() != null){
            sql.append("mat_unit=\""+mat.getMat_unit()+"\",");
        }
        //物资描述
        if (mat.getMat_desc() != null){
            sql.append("mat_desc=\""+mat.getMat_desc()+"\",");
        }

        if (sql.lastIndexOf(",") != -1){//去掉末尾逗号
            sql.deleteCharAt(sql.lastIndexOf(","));
        }else{
            //没有修改数据
            return "";
        }
        return sql.toString()+" where mat_id=\""+mat.getMat_id()+"\"";
    }
}
