package com.qsdbl.nazox_demo.utils;

import java.math.BigDecimal;

/**
 * @description 数学工具类
 * @author 轻率的保罗
 * @since 2021-07-28
 */
public class MyMath {
    /**
     * @description 设置保留n位 有效小数位，四舍五入
     * @param target 要转换的数字
     * @param num 小数点位数
     */
    public static String setDecimals(double target,int num) {
        BigDecimal b =new BigDecimal(target);
        double f1 = b.setScale(num, BigDecimal.ROUND_HALF_UP).doubleValue();
        return f1 +"";
    }
    public static String setDecimals(String target,int num) {
        return setDecimals(Double.valueOf(target),num);
    }

    /**
     * 设置保留n位 有效小数位，四舍五入
     * 并去掉多余的0
     */
    public static String setDecimals_int(double target) {
        return subZeroAndDot(setDecimals(target,0) +"");
    }
    public static String setDecimals_int(String target) {
        return setDecimals_int(Double.valueOf(target));
    }

    /**
     * 使用java正则表达式去掉多余的.与0
     */
    public static String subZeroAndDot(Object target) {
        String s = String.valueOf(target);
        if (s.indexOf(".") >0) {
            //去掉多余的0
            s = s.replaceAll("0+?$","");
            //如最后一位是.则去掉
            s = s.replaceAll("[.]$","");
        }
        return s;
    }
}
