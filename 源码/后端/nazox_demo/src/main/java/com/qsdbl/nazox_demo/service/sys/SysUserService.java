package com.qsdbl.nazox_demo.service.sys;

import com.alibaba.fastjson.JSONObject;
import com.qsdbl.nazox_demo.entity.vo.DataVo;
import com.qsdbl.nazox_demo.mapper.sys.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @description 用户表 服务层
 * @author 轻率的保罗
 * @since 2021-08-26
 */
@Service
public class SysUserService {

    /**
     * 用户表 Mapper对象
     */
    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * 查询所有用户数据
     */
    public DataVo queryAll() {
        DataVo dataVo = new DataVo();//视图对象，按规定好的格式返回数据给前端
        dataVo.setData(sysUserMapper.queryAll());
        return dataVo;
    }

}
