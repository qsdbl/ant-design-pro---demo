package com.qsdbl.nazox_demo.web.sys;

import com.alibaba.fastjson.JSONObject;
import com.qsdbl.nazox_demo.entity.vo.DataVo;
import com.qsdbl.nazox_demo.service.sys.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @description 用户表 控制层
 * @author 轻率的保罗
 * @since 2021-08-26
 */
@RestController
//@RequestMapping("/nazox_demo/v1/")
@CrossOrigin
@Api(tags = "用户表相关操作")//swagger注解，用于给当前模块添加注释
public class SysUserController {

    /**
     * 用户表 Service对象
     */
    @Autowired
    private SysUserService sysUserService;

    /**
     * 查询所有用户数据
     */
    @ApiOperation("查询所有用户数据")//swagger注解，用于给当前api（接口）添加注释
    @GetMapping("/nazox_demo/v1/sysusers")
    public DataVo queryAll(){//@ApiParam - swagger注解，用于给参数添加注释
        return sysUserService.queryAll();
    }

    /**
     * 用于演示ant design pro
     */
    @RequestMapping("/api/login/account")
    public String api(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("currentAuthority","admin");
        jsonObject.put("status","ok");
        jsonObject.put("type","account");
        return jsonObject.toString();
    }

    /**
     * 用于演示ant design pro
     */
    @RequestMapping("/api/currentUser")
    public String api2(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data","{\"name\":\"Serati Ma\",\"avatar\":\"https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png\",\"userid\":\"00000001\",\"email\":\"antdesign@alipay.com\",\"signature\":\"海纳百川，有容乃大\",\"title\":\"交互专家\",\"group\":\"蚂蚁金服－某某某事业群－某某平台部－某某技术部－UED\",\"tags\":[{\"key\":\"0\",\"label\":\"很有想法的\"},{\"key\":\"1\",\"label\":\"专注设计\"},{\"key\":\"2\",\"label\":\"辣~\"},{\"key\":\"3\",\"label\":\"大长腿\"},{\"key\":\"4\",\"label\":\"川妹子\"},{\"key\":\"5\",\"label\":\"海纳百川\"}],\"notifyCount\":12,\"unreadCount\":11,\"country\":\"China\",\"access\":\"admin\",\"geographic\":{\"province\":{\"label\":\"浙江省\",\"key\":\"330000\"},\"city\":{\"label\":\"杭州市\",\"key\":\"330100\"}},\"address\":\"西湖区工专路 77 号\",\"phone\":\"0752-268888888\"}");
        jsonObject.put("success","true");
        return jsonObject.toString();
    }
}