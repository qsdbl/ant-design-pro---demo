package com.qsdbl.nazox_demo.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;

/**
 * @description vo - 视图对象
 */
@Data
@ApiModel("返回数据给前端的视图对象")
public class DataVo<T> {
    @ApiModelProperty("请求处理状态。true-成功，false-失败。")
    private String success = "true";
    @ApiModelProperty("返回的消息。一般为请求处理失败后返回的错误提示。")
    private String message;
    @ApiModelProperty("请求处理成功返回的数据")
    private T data;
}

