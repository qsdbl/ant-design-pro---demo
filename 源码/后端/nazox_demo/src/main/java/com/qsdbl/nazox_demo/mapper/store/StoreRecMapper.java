package com.qsdbl.nazox_demo.mapper.store;

import com.qsdbl.nazox_demo.entity.StoreIn;
import com.qsdbl.nazox_demo.entity.StoreRec;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @description 库存查询表 Mapper层
 * @author 轻率的保罗
 * @since 2021-08-27
 */
@Mapper
public interface StoreRecMapper {

    //添加 库存查询数据
    @Insert("insert into store_rec(store_id,last_date,last_code,store_money,store_num,store_price,mat_unit,mat_size,mat_name,mat_code,mat_id,house_id,house_code,house_name,add_date,modify_date) values(#{store_id},#{last_date},#{last_code},#{store_money},#{store_num},#{store_price},#{mat_unit},#{mat_size},#{mat_name},#{mat_code},#{mat_id},#{house_id},#{house_code},#{house_name},#{add_date},#{modify_date})")
    public int insert(StoreRec storeRec);

    //更新 库存查询 数量、库存金额（数量乘单价）。 house_id、mat_id
    @Update("update store_rec set store_num=#{store_num},store_price=#{store_price},store_money=#{store_money},last_date=#{last_date},last_code=#{last_code},modify_date=#{modify_date} where house_id = #{house_id} and mat_id = #{mat_id}")
    public int update(StoreRec storeRec);

    //查询所有
    @Select("select * from store_rec order by Last_date desc")
    public List<StoreRec> queryAll();

    //通过 主键 查询
    @Select("select * from store_rec where store_id = #{store_id}")
    public StoreRec queryByID(@Param("store_id") String store_id);

    //通过 仓库id、物资id 查询
    @Select("select * from store_rec where house_id = #{house_id} and mat_id = #{mat_id}")
    public StoreRec queryByHM(@Param("house_id") String house_id,@Param("mat_id") String mat_id);
}
