package com.qsdbl.nazox_demo.utils;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import java.security.KeyPair;

/**
 * RSA工具类，基于hutool进行封装
 * 公钥、私钥保存在springboot配置文件中。使用下边方式生成：
 * KeyPair pair = SecureUtil.generateKeyPair("RSA");
 * String publickey = Base64.encode(pair.getPublic().getEncoded());
 * String privatekey = Base64.encode(pair.getPrivate().getEncoded());
 *
 * @author 轻率的保罗
 * @since 2021-12-14
 */
@Component
public class RSAtool {
    //公钥
    @Value("${rsa.publicKey}")
    private String publicKey;
    //私钥
    @Value("${rsa.privateKey}")
    private String privateKey;
    @Autowired
    private RSA rsa;

    //创建RSA对象
    @Bean
    private RSA init() {
        if (publicKey.length() <= 10 || privateKey.length() <= 10) {
            //公钥、密钥长度必须大于10位，否则创建新的密钥对
            KeyPair pair = SecureUtil.generateKeyPair("RSA");
            publicKey = Base64.encode(pair.getPublic().getEncoded());
            privateKey = Base64.encode(pair.getPrivate().getEncoded());
        }
        return new RSA(privateKey, publicKey);
    }

    //获得公钥
    public String getPublicKey() {
        return publicKey;
    }

    //加密：
    //公钥加密
    public String encrypt(String str) {
        byte[] encrypt = rsa.encrypt(StrUtil.bytes(str, CharsetUtil.CHARSET_UTF_8), KeyType.PublicKey);
        //返回base64编码的字符串
        return Base64.encode(encrypt);
    }

    // 私钥解密
    public String decrypt(String str) {
        byte[] decrypt = rsa.decrypt(Base64.decode(str), KeyType.PrivateKey);
        return StrUtil.str(decrypt, CharsetUtil.CHARSET_UTF_8);
    }

    //签名：
    //私钥加密
    public String encryptSign(String str) {
        byte[] encrypt = rsa.encrypt(StrUtil.bytes(str, CharsetUtil.CHARSET_UTF_8), KeyType.PrivateKey);
        //返回base64编码的字符串
        return Base64.encode(encrypt);
    }

    // 公钥解密
    public String decryptSign(String str) {
        byte[] decrypt = rsa.decrypt(Base64.decode(str), KeyType.PublicKey);
        return StrUtil.str(decrypt, CharsetUtil.CHARSET_UTF_8);
    }
}

