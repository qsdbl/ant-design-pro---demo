package com.qsdbl.nazox_demo.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

/**
 * @description 出库单表 实体类
 * @author 轻率的保罗
 * @since 2021-08-27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("出库单")
public class StoreOut {
    @ApiModelProperty("单号")
    private String out_code;
    @ApiModelProperty("状态")
    private String auditing;
    @ApiModelProperty("出库日期")
    private String out_date;
    @ApiModelProperty("仓库ID")
    private String house_id;
    @ApiModelProperty("仓库")
    private String house_name;
    @ApiModelProperty("仓管员")
    private String edit_user;
    @ApiModelProperty("仓管员ID")
    private String edit_userid;
    @ApiModelProperty("领料人")
    private String send_user;
    @ApiModelProperty("出库说明")
    private String out_desc;
    @ApiModelProperty("出库金额")
    private String out_money;
    @ApiModelProperty("主键")
    private String out_id;
    @ApiModelProperty("添加人ID")
    private String add_userid;
    @ApiModelProperty("添加时间")
    private String add_date;
    @ApiModelProperty("修改人ID")
    private String modify_userid;
    @ApiModelProperty("修改时间")
    private String modify_date;
    @ApiModelProperty("系统租户ID")
    private String tenant_id;
    @ApiModelProperty("物资列表")
    private List<StoreMat> mats;
    @ApiModelProperty("修改人姓名。查询出库单数据时使用到")
    private String modify_username;
    @ApiModelProperty("仓库编码。创建出库单时用于接收数据")
    private String house_code;
    @ApiModelProperty("制单员")
    private String pur_user;
    @ApiModelProperty("制单员ID")
    private String pur_userid;
}
