package com.qsdbl.nazox_demo.web.store;

import com.qsdbl.nazox_demo.entity.StoreHouse;
import com.qsdbl.nazox_demo.entity.vo.DataVo;
import com.qsdbl.nazox_demo.service.store.StoreHouseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @description 仓库目录表 控制层
 * @author 轻率的保罗
 * @since 2021-08-27
 */
@RestController
@RequestMapping("/nazox_demo/v1/")
@CrossOrigin
@Api(tags = "仓库目录表相关操作")
public class StoreHouseController {

    /**
     * 仓库目录表 Service对象
     */
    @Autowired
    private StoreHouseService storeHouseService;

    @ApiOperation("新增仓库")
    @PostMapping("storehouse/{house_code}")
    public DataVo insert(@RequestBody @ApiParam("仓库数据，必须提供编码house_code、名称house_name") StoreHouse house){//@ApiParam - swagger注解，用于给参数添加注释
        return storeHouseService.insert(house);
    }

    @ApiOperation("删除")
    @DeleteMapping("storehouse/{house_id}")
    public DataVo delete(@PathVariable @ApiParam("仓库id") String house_id){
        return storeHouseService.delete(house_id);
    }

    @ApiOperation("修改仓库信息")
    @PatchMapping("storehouse/{house_code}")
    public DataVo update(@RequestBody  @ApiParam("新的仓库信息，必须提供id") StoreHouse house){
        return storeHouseService.update(house);
    }

    @ApiOperation("查询仓库目录")
    @GetMapping("storehouses")
    public DataVo queryAll_house(){
        return storeHouseService.queryAll_house();
    }
}