package com.qsdbl.nazox_demo.service.store;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qsdbl.nazox_demo.entity.StoreIn;
import com.qsdbl.nazox_demo.entity.StoreIndet;
import com.qsdbl.nazox_demo.entity.StoreMat;
import com.qsdbl.nazox_demo.entity.StoreRec;
import com.qsdbl.nazox_demo.entity.qo.PageInfo_qo;
import com.qsdbl.nazox_demo.entity.vo.DataVo;
import com.qsdbl.nazox_demo.mapper.store.StoreInMapper;
import com.qsdbl.nazox_demo.mapper.store.StoreIndetMapper;
import com.qsdbl.nazox_demo.mapper.store.StoreRecMapper;
import com.qsdbl.nazox_demo.utils.GenerateID;
import com.qsdbl.nazox_demo.utils.MyMath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @description 入库单表 服务层
 * @author 轻率的保罗
 * @since 2021-07-18
 */
@Service
public class StoreInService {

    /**
     * 入库单表 Mapper对象
     */
    @Autowired
    private StoreInMapper storeInMapper;

    /**
     * 入库明细表 Mapper对象
     */
    @Autowired
    private StoreIndetMapper indetMapper;

    /**
     * 库存查询表 Mapper对象
     */
    @Autowired
    private StoreRecMapper recMapper;

    /**
     * @description 新增 入库单（需要更新 入库单、入库明细表、库存查询表）
     * @param  storeIn 入库单信息
     * @param  storeMats 入库明细数据
     */
    @Transactional//开启事务
    public DataVo insert(StoreIn storeIn,StoreMat[] storeMats){
        DataVo dataVo = new DataVo();//视图对象，按规定好的格式返回数据给前端
        //生成 单号
        String in_code = GenerateID.storeInCode("RK");
        while (storeInMapper.queryByCode(in_code)!=null){
            in_code = GenerateID.storeInCode("RK");
        }
        storeIn.setIn_code(in_code);
        //生成 入库ID
        String in_id = GenerateID.storeMatID();
        while (storeInMapper.queryByID(in_id)!=null){
            in_id = GenerateID.storeMatID();
        }
        String now_date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        storeIn.setModify_date(now_date);
        storeIn.setAdd_date(now_date);
        storeIn.setIn_date(now_date);
        storeIn.setIn_id(in_id);
        storeIn.setModify_userid(storeIn.getPur_userid());//新增数据时 制单人就是修改人
        storeIn.setAdd_userid(storeIn.getPur_userid());//新增数据时 制单人就是添加人
        if(storeInMapper.insert(storeIn)<=0){//新增入库单
            dataVo.setSuccess("false");
            dataVo.setMessage("入库单,添加失败！");
            return dataVo;
        }
        return updateIM(storeIn,storeMats,in_id,in_code,now_date,dataVo);//更新 入库明细、库存查询
    }

    /**
     * @description 更新 入库明细、库存查询
     * @param storeMats 入库单所包含的物资
     * @param in_id 入库单 主键
     * @param in_code 入库单 单号
     */
    @Transactional//开启事务
    public DataVo updateIM(StoreIn storeIn,StoreMat[] storeMats,String in_id,String in_code,String now_date,DataVo dataVo){
        for (int i = 0; i < storeMats.length; i++) {
            //新增入库明细：
            StoreIndet indet = new StoreIndet();
            //生成 入库明细ID
            String in_detid = GenerateID.storeMatID();
            while (indetMapper.queryByID(in_detid)!=null){
                in_detid = GenerateID.storeMatID();
            }
            indet.setIn_detid(in_detid);
            indet.setMat_id(storeMats[i].getMat_id());
            indet.setIn_num(storeMats[i].getIn_num());
            indet.setIn_money(storeMats[i].getIn_money());
            indet.setLocal_code(storeMats[i].getLocal_code());
            indet.setIn_id(in_id);
            if(indetMapper.insert(indet)<=0){
                dataVo.setSuccess("false");
                dataVo.setMessage("入库明细,添加失败！");
                return dataVo;
            }
            //新增 库存查询：
            StoreRec rec_db = recMapper.queryByHM(storeIn.getHouse_id(), storeMats[i].getMat_id());//查看是否已有记录
            StoreRec rec = new StoreRec();
            if (rec_db != null){
                rec = rec_db;
            }
            //生成 库存查询ID
            String store_id = GenerateID.storeMatID();
            while (recMapper.queryByID(store_id)!=null){
                store_id = GenerateID.storeMatID();
            }
            rec.setStore_id(store_id);
            rec.setLast_code(in_code);
            rec.setLast_date(now_date);
            rec.setAdd_date(now_date);
            rec.setModify_date(now_date);
            rec.setMat_unit(storeMats[i].getMat_unit());
            rec.setMat_size(storeMats[i].getMat_size());
            rec.setMat_name(storeMats[i].getMat_name());
            rec.setMat_code(storeMats[i].getMat_code());
            rec.setMat_id(storeMats[i].getMat_id());
            rec.setHouse_id(storeIn.getHouse_id());
            rec.setHouse_code(storeIn.getHouse_code());
            rec.setHouse_name(storeIn.getHouse_name());
            if(rec_db == null){
                //新增数据
                rec.setStore_num(storeMats[i].getIn_num());//数量
                rec.setStore_price(storeMats[i].getMat_price());//物资单价
                rec.setStore_money(MyMath.subZeroAndDot(Double.valueOf(storeMats[i].getIn_num())*Double.valueOf(storeMats[i].getMat_price())));
                if(recMapper.insert(rec)<=0){
                    dataVo.setSuccess("false");
                    dataVo.setMessage("库存查询,添加失败！");
                    return dataVo;
                }
            }else{
                //更新数据
                rec.setStore_num(MyMath.subZeroAndDot(Double.valueOf(rec_db.getStore_num())+Double.valueOf(storeMats[i].getIn_num())));
                rec.setStore_price(storeMats[i].getMat_price());//物资单价
                rec.setStore_money(MyMath.subZeroAndDot(Double.valueOf(rec_db.getStore_num())*Double.valueOf(rec_db.getStore_price())));
                if(recMapper.update(rec)<=0){
                    dataVo.setSuccess("false");
                    dataVo.setMessage("库存查询,更新数据失败！");
                    return dataVo;
                }
            }
        }
        dataVo.setMessage("添加成功！");
        return dataVo;
    }

    /**
     * @description 分页查询 入库单数据
     * @param qo 分页查询相关的数据，包括每页显示的数据大小pageSize（默认为10）、查询的当前页currentPage（默认为1）
     * @return 入库单详细数据（包含物资）
     */
    public DataVo queryAll(PageInfo_qo qo) {
        //设置对应的分页参数
        //第一个参数pageNum：需要查询的当前页。pageSize：需要显示的数据量大小
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        //查询
        List<StoreIn> list = storeInMapper.queryAll();
        //封装成分页数据
        PageInfo pageInfo = new PageInfo(list);
        DataVo dataVo = new DataVo();//视图对象，按规定好的格式返回数据给前端
        dataVo.setData(pageInfo);
        return dataVo;
    }

}
