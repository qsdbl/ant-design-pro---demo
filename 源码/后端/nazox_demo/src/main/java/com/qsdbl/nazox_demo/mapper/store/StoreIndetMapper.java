package com.qsdbl.nazox_demo.mapper.store;

import com.qsdbl.nazox_demo.entity.StoreIndet;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @description 入库明细表 Mapper层
 * @author 轻率的保罗
 * @since 2021-08-26
 */
@Mapper
public interface StoreIndetMapper {

    //添加 入库明细
    @Insert("insert into store_indet(mat_id,in_num,in_money,in_id,local_code,in_detid) values(#{mat_id},#{in_num},#{in_money},#{in_id},#{local_code},#{in_detid})")
    public int insert(StoreIndet storeIndet);

    //通过 入库明细ID 查询
    @Select("select * from store_indet where in_detid = #{in_detid}")
    public StoreIndet queryByID(@Param("in_detid") String in_detid);

    //通过 物资ID 查询 货位
    @Select("select local_code from store_indet where mat_id = #{mat_id} and local_code is not null group by local_code")
    public List<String> queryByLC(@Param("mat_id") String mat_id);
}
