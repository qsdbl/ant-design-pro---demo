package com.qsdbl.nazox_demo;

import com.alibaba.druid.pool.DruidDataSource;
import com.qsdbl.nazox_demo.utils.RSAtool;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;


/**
 * @author 轻率的保罗
 * @since 2022/4/12
 */
@SpringBootTest
public class MyTest02 {

    @Autowired
    DataSource dataSource;
    @Autowired
    RSAtool rsAtool;

    @Test
    public void test() throws SQLException {

//        //看一下默认数据源
//        System.out.println(dataSource.getClass());
//        //获得连接
//        Connection connection = null;
//        connection = dataSource.getConnection();
//        System.out.println(connection);
//
//        DruidDataSource druidDataSource = (DruidDataSource) dataSource;
//        System.out.println("druidDataSource 数据源最大连接数：" + druidDataSource.getMaxActive());
//        System.out.println("druidDataSource 数据源初始化连接数：" + druidDataSource.getInitialSize());
//
//        //关闭连接
//        connection.close();



//        System.out.println(QrCodeUtil.generateAsBase64("hello world",QrConfig.create().setImg("C:/Users/D_HH/Desktop/picture.jpg"),"jpg"));
//        QrCodeUtil.generate("hello world", 300, 300, FileUtil.file("C:/Users/D_HH/Desktop/qrcode.jpg"));


//        KeyPair pair = SecureUtil.generateKeyPair("RSA");
//        String publickey = Base64.encode(pair.getPublic().getEncoded());
//        String privatekey = Base64.encode(pair.getPrivate().getEncoded());
//        System.out.println("私钥 =\n"+ privatekey);
//        System.out.println("公钥 =\n"+publickey);


//        Calendar d1 = Calendar.getInstance();
//        Calendar d2 = Calendar.getInstance();
//        //差异时间，单位秒
//        long diff = (d1.getTimeInMillis() - d2.getTimeInMillis())/1000;


//        String msg = "hello word！你好！";
//        System.out.println("公钥 = \n"+rsAtool.getPublicKey());
//
//        System.out.println("\n--加密--");
//        Calendar c1 = Calendar.getInstance();
//        String en = rsAtool.encrypt(msg);
//        Calendar c2 = Calendar.getInstance();
//        long diff = c2.getTimeInMillis() - c1.getTimeInMillis();
//        System.out.println("公钥加密，耗时 "+diff+" 毫秒。密文 = \n"+en);
//        Calendar c3 = Calendar.getInstance();
//        String de = rsAtool.decrypt(en);
//        Calendar c4 = Calendar.getInstance();
//        long diff2 = c4.getTimeInMillis() - c3.getTimeInMillis();
//        System.out.println("私钥解密，耗时 "+diff2+" 毫秒。原文 = \n"+de);
//
//
//        System.out.println("\n--签名--");
//        String en2 = rsAtool.encryptSign(msg);
//        System.out.println("私钥加密 = \n"+en2);
//        System.out.println("公钥解密 = \n"+rsAtool.decryptSign(en2));



//        System.out.println(DateUtil.now());


    }
}
