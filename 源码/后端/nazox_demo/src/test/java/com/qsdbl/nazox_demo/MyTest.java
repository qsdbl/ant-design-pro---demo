package com.qsdbl.nazox_demo;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.qsdbl.nazox_demo.utils.RSAtool;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import javax.sql.DataSource;
import java.sql.SQLException;


@SpringBootTest
class MyTest {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void test() throws SQLException {
        //redisTemplate 操作不同的数据类型
        //ops，operations，操作
        //opsForValue 操作字符串
        //opsForList 操作List
        //opsForSet
        //opsForHash
        //。。。

        //redisTemplate.opsForValue().set("name","小黑");
        //除了基本的操作，我们常用的方法都可以直接通过redisTemplate操作，比如事务、基本的CRUD

        //获取redis的连接对象
        //RedisConnection connection = redisTemplate.getConnectionFactory().getConnection();
        //connection.flushDb();//清空当前数据库的所有key

        //示例：
//        redisTemplate.opsForValue().set("age","18");
//        redisTemplate.opsForValue().set("addr","北京朝阳zhaoyan");
//        System.out.println(redisTemplate.opsForValue().get("age"));
//        System.out.println(redisTemplate.opsForValue().get("addr"));


//        Dog dog = new Dog();
    }
}

//class Animal{
//    Animal(){
//        System.out.println("父类构造器！！！");
//    }

//    Animal(String str){
//        System.out.println("父类构造器！！！"+str);
//    }
//
//    public void run(){
//        System.out.println("run");
//    }
//}
//
//class Dog extends Animal{
//    Dog(){
////        super("hello");
//        System.out.println("子类构造器！！！");
//    }
//}